CC=g++
OS=32
LDFLAGS =
CFLAGS = -std=c++11 -O2
SHARED_EXT=so
INCLUDE =

MINGW_LDFLAGS = -L./lib -lmingw32 -lboost_system -lboost_filesystem -lboost_chrono -lSDL_gfx ./lib/SDL_image.lib -lSDLmain -lSDL.dll -lglew32.dll -lglu32 -lopengl32 -g -lm
LINUX_LDFLAGS = -lSDL -lSDL_gfx -lSDL_image -lGLEW -lGLU -lGL -lboost_system -lboost_filesystem -lboost_chrono -lm

FIND_TOOL=find
SYS := $(shell $(CC) -dumpmachine)
ifneq (, $(findstring linux, $(SYS)))
   FIND_TOOL = find
   LDFLAGS = $(LINUX_LDFLAGS)
else ifneq (, $(findstring mingw, $(SYS)))
   FIND_TOOL = /bin/find
   LDFLAGS = $(MINGW_LDFLAGS)
   INCLUDE = -I./include
   SHARED_EXT = dll
endif

LBITS := $(shell uname -m)
ifeq ($(LBITS), x86_64)
   OS = 64
endif

SRCS = $(shell $(FIND_TOOL) ./src -type f -name "*.cpp" |tr "\\n" " ")
OBS = $(shell $(FIND_TOOL) ./src -type f -name "*.cpp" |sed "s/.cpp/.o/g" |tr "\\n" " ")

all: linux

linux: $(OBS)
	$(CC) $(OBS) $(LDFLAGS) -o ./bin/C2-linux_$(OS)

windows: $(OBS)
	$(CC) $(OBS) $(LDFLAGS) -o ./bin/C2-win_$(OS)

library:
	$(MAKE) -f lib.mk

%.o: %.cpp
	$(CC) $(CFLAGS) -c $(INCLUDE) $< -o $@

debug:
	$(CC) $(SRCS) $(CFLAGS) $(LDFLAGS) -o ./bin/debug

clean:
	@echo $(OBS) |xargs rm -f
	
test:
	echo $(OBS)