# Block structure
## functions

```c++
type foo {
	..code..
}
```

or if very short and single statement (getters/setters)

```c++
type foo { ..code..}
```

## flow
```c++
while/if/for/etc {
	..code..
}
```

or if single statement

```c++
while/if/for/etc
	..code..
```

# Headers
Public before private and functions before member variables.