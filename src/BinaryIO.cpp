#include "BinaryIO.h"

/**
 * write one four bytes long integer to file
 */
bool BinaryIO::writeInt(int val) {
	charInt cVal;
	cVal.num = val;

	if(file == NULL) return false;

	fwrite(cVal.bytes, 1, sizeof(int), file);

	return true;
}

/**
 * Write one two bytes long integer to file
 * @param val
 * @return 
 */
bool BinaryIO::writeShort(short int val) {
	charShort cVal;
	cVal.num = val;

	if(file == NULL) return false;

	fwrite(cVal.bytes, 1, sizeof(short int), file);

	return true;
}

/**
 * write one byte to file
 * @param val
 * @return 
 */
bool BinaryIO::writeChar(char val) {
	if(file == NULL) return false;

	fwrite(&val, 1, 1, file);
}

/**
 * Deprecated because useless now. Will be removed.
 */
void BinaryIO::swapByteOrder(unsigned char * target, int size) {
	unsigned char temp[size];

	memcpy(&temp, target, size);

	for(int i = 1; i <= size; i++) {
		target[i-1] = temp[size-i];
	}
}

/**
 * open file for writing
 * @param filename
 * @return 
 */
bool BinaryIO::open(std::string filename) {
	file = std::fopen(const_cast<char*>(filename.c_str()), "wb");

}

/**
 * close opened file
 */
void BinaryIO::close() {
	std::fclose(file);
}

/**
 * read file to buffer for further examination
 * @param filename
 * @return True if the file was read successfully
 */
bool BinaryIO::read(std::string filename) {
	std::ifstream mapfile;
	mapfile.open(filename.c_str(), std::ios::binary|std::ios::in|std::ios::ate);
	
	if(mapfile.fail() == true){
		return false;
	}
	std::ifstream::pos_type size;

	if(mapfile.is_open()) {
		size = mapfile.tellg();
		data = new char[size];
		mapfile.seekg(0, std::ios::beg);
		mapfile.read(data, size);
		mapfile.close();
		mapSize = (int)size;
		cursor = 0;
		EndOfFile = false;
		
		return true;
	}
	
	return false;
}

/**
 * return next character from file, if available
 * @return char 
 */
char BinaryIO::nextChar() {
	cursor++;
	if(cursor < mapSize) {
		return data[cursor-1];
	}
	else {
		EndOfFile = true;
		return 0;
	}
}

/**
 * return null-terminated string from buffer
 * @return char* 
 */
std::string BinaryIO::nextString() {
	std::string str;
	char c;

	while((c = nextChar()) != 0) {
		str += c;
	}

	return str;
}

/**
 * return four bytes long integer from buffer
 * @return int 
 */
int BinaryIO::nextInt() {
	charInt i;
	i.bytes[0] = nextChar();
	i.bytes[1] = nextChar();
	i.bytes[2] = nextChar();
	i.bytes[3] = nextChar();
	return i.num;
}

/**
 * return two bytes long integer from buffer
 * @return 
 */
short BinaryIO::nextShort() {
	charShort s;
	s.bytes[0] = nextChar();
	s.bytes[1] = nextChar();

	return s.num;
}
