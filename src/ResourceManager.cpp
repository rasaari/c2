#include "ResourceManager.h"
#include <SDL/SDL_image.h>
#include "BinaryIO.h"
#include "Utility.h"
#include <GL/glew.h>

const std::string ResourceManager::_tilesetPath = "./data/gfx/tilesets/";
const std::string ResourceManager::_spritePath = "./data/gfx/sprites/";
const std::string ResourceManager::_imagePath = "./data/gfx/images/";
const std::string ResourceManager::_fontPath = "./data/gfx/fonts/";
std::map<std::string, ResourceManager::imageData> ResourceManager::_glImages;
std::map<std::string, ResourceManager::spriteData> ResourceManager::_glSpritesData;
std::map<std::string, ResourceManager::animation> ResourceManager::_animEnumStrings;
std::map<std::string, ResourceManager::fontData> ResourceManager::_glFontsData;

UV4::UV4(){
}

UV4::UV4(float w, float h, int id, int tileSizeX, int tileSizeY) {
	float ufrac = tileSizeX / w;
	float vfrac = tileSizeY / h;
	int uTiles = (w / tileSizeX);
	//int vTiles = (h/tileSizeY);
	float col = id % uTiles;
	float row = floorf(id / uTiles);

	u0 = ufrac*col;
	v0 = vfrac*row;
	u1 = u0 + ufrac;
	v1 = v0 + vfrac;
}

ResourceManager::fontData::fontData() : letterDimensions(Vec2i(0,0)), kerning(0){
}

void ResourceManager::init(){
	loadBlankImage();
	
	_animEnumStrings["walk_0"]	= WALK_0;
	_animEnumStrings["walk_45"]	= WALK_45;
	_animEnumStrings["walk_90"]	= WALK_90;
	_animEnumStrings["walk_135"]	= WALK_135;
	_animEnumStrings["walk_180"]	= WALK_180;
	_animEnumStrings["walk_225"]	= WALK_225;
	_animEnumStrings["walk_270"]	= WALK_270;
	_animEnumStrings["walk_315"]	= WALK_315;
	
	_animEnumStrings["idle_0"]	= IDLE_0;
	_animEnumStrings["idle_45"]	= IDLE_45;
	_animEnumStrings["idle_90"]	= IDLE_90;
	_animEnumStrings["idle_135"]	= IDLE_135;
	_animEnumStrings["idle_180"]	= IDLE_180;
	_animEnumStrings["idle_225"]	= IDLE_225;
	_animEnumStrings["idle_270"]	= IDLE_270;
	_animEnumStrings["idle_315"]	= IDLE_315;
}

/**
 * Loads an image file from disk.
 * 
 * @param alias		ResourceManager alias
 * @param source	Path of the file
 * @param tileSizeX	
 * @param tileSizeY
 * @return		True if the texture was successfully loaded. False otherwise.
 */
bool ResourceManager::_loadImage(std::string alias, std::string source, int tileSizeX, int tileSizeY){
	/* If the alias already exists, free the existing image first */
	if(_glImages.find(alias) != _glImages.end()){
		freeImage(alias);
	}
	
	SDL_Surface *img = IMG_Load(source.c_str());
	GLuint texture;
	
	if(img){
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);
		
		glTexImage2D(GL_TEXTURE_2D, 0, img->format->BytesPerPixel, img->w, img->h, 0, (img->format->BytesPerPixel == 3) ? GL_RGB : GL_RGBA, GL_UNSIGNED_BYTE, img->pixels);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);

		imageData data(texture, Vec2i(img->w, img->h), Vec2i(tileSizeX, tileSizeY));
		_glImages[alias] = data;
		
		SDL_FreeSurface(img);
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	else{
		std::cerr << "Failed to load image: " << IMG_GetError() << "\n";
		return false;
	}

	return true;
}

/**
 * Loads a tileset texture from ResourceManager::tilesetPath
 * 
 * @param alias		ResourceManager alias
 * @param filename	Filename of the tileset
 * @param tileSize	Tileset tile side length
 * @return		false on failure
 */
bool ResourceManager::loadTilesetTexture(std::string alias, std::string filename, int tileSize){
	filename = _tilesetPath+filename;
	return _loadImage(alias, filename, tileSize, tileSize);
}

/**
 * Loads a spriteset texture and animations from ResourceManager::spritePath
 * 
 * @param alias		ResourceManager alias
 * @param filename	Filename of the sprite
 * @param tileSizeX	Sprite tile width
 * @param tileSizeY	Sprite tile height
 * @return		false on failure
 */
bool ResourceManager::loadSpriteTexture(std::string alias, std::string filename){
	filename = _spritePath+filename;
	
	if(_importSpriteData(alias, filename) == true){
		if(_loadImage(alias, filename, _glSpritesData[alias].spriteWidth, _glSpritesData[alias].spriteHeight) == true){
			return true;
		}
		else{
			freeImage(alias);
		}
	}
	
	return false;
}

/**
 * Loads an image texture from ResourceManager::imagePath
 * 
 * TODO: everything
 * 
 * @param alias
 * @param filename
 * @return 
 */
bool ResourceManager::loadImageTexture(std::string alias, std::string filename){
	return true;
}

/**
 * Loads a font exture from ResourceManager::fontPath
 * 
 * 
 * @param alias
 * @param filename
 * @return 
 */
bool ResourceManager::loadFontTexture(std::string alias, std::string filename, int kerning){
	filename = _fontPath+filename;
	if(_loadImage(alias, filename) == true){
		_setFontData(alias,kerning);
	}
	
	return false;
}

/**
 * Loads animation data from .c2anim file.
 * 
 * TODO: io error handling
 * 
 * @param alias ResourceManager alias
 * @return false on failure
 */
bool ResourceManager::_importSpriteData(std::string alias, std::string imgFilePath){
	_glSpritesData[alias];
	
	std::vector<int> tempIds;
	std::string animString, filePath;
	int spriteWidth, spriteHeight, frameCount, dotIndex;
	
	dotIndex = imgFilePath.find_last_of('.');
	filePath = imgFilePath.substr(0,dotIndex); 
	
	BinaryIO bin;
	
	//path to anim file
	std::string path = filePath+".c2anim";
	bin.read(path);
	
	spriteWidth = bin.nextShort();
	spriteHeight = bin.nextShort();
	_glSpritesData[alias].spriteWidth = spriteWidth;
	_glSpritesData[alias].spriteHeight = spriteHeight;
	std::cout << path << " " << spriteWidth << " " << spriteHeight << "\n";
	while(!bin.EndOfFile) {
		animString = bin.nextString();
		frameCount = bin.nextShort();
		for (int i = 0; i < frameCount; i++) {
			tempIds.push_back(bin.nextShort());
		}
		
		_glSpritesData[alias].add(_animEnumStrings[animString],tempIds);
		tempIds.clear();
	}
	
	return true;
}

/**
 * Recalculates font texture uv-coordinates and a sets the font kerning.
 * 
 * @param alias Resourcemanager alias
 * @param kerning How many pixels each letter is adjusted in x-axis.
 */
void ResourceManager::_setFontData(const std::string &alias, int kerning){
	Vec2i textureDimensions, letterDimensions;
	
	textureDimensions = getImageDimensions(alias);
	letterDimensions = textureDimensions/16;
	
	imageData data(getImage(alias), textureDimensions, letterDimensions);
	_glImages[alias] = data;
	
	fontData fData;
	fData.letterDimensions = letterDimensions;
	fData.kerning = letterDimensions.x+kerning;
	_glFontsData[alias] = fData;
}

/**
 * Frees the image and its data from memory. Silently ignores unknown images.
 * 
 * TODO: free special data.
 */
void ResourceManager::freeImages() {
	while(_glImages.empty() == false){
		_glImages.begin()->second.free();
		_glImages.erase(_glImages.begin()->first);
	}
	glFinish();
}

/**
 * Frees all loaded tilesets/sprites/images and their data from memory.
 * 
 * TODO: free special data.
 */
void ResourceManager::freeImage(const std::string &alias) {
	glImageIt it = _glImages.find(alias);
	if(it != _glImages.end()){
		it->second.free();
		_glImages.erase(it->first);
	}
}

/**
 * Returns an OpenGL texture resource matching the supplied alias. If the texture is not found a grey texture is returned.
 * 
 * @param alias ResourceManager alias
 * @return The texture.
 */
GLuint ResourceManager::getImage(const std::string &alias) {
	glImageIt it = _glImages.find(alias);
	return (it == _glImages.end())?_glImages["blank"].id():_glImages[alias].id();
}

/**
 * Adds a grey texture with alias "blank" ResourceManager::glImages
 */
void ResourceManager::loadBlankImage(){
	unsigned char* arr;
	int w = 96;
        int h = 96;
	arr = new unsigned char[w * h * 4];
	
	for(int i = 0; i < (w * h * 4); i++){
		arr[i] = (i%4 == 3)?255:128;
        }
	
	GLuint texture;
        glGenTextures(1, &texture);
        glBindTexture(GL_TEXTURE_2D, texture);
	
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, arr);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);
	
	delete[] arr;
	
	imageData data(texture,Vec2i(96,96),Vec2i(48,48));

	_glImages["blank"] = data;
}

/**
 * Returns the chosen image dimension. If image is not found, 0 is returned.
 * 
 * @param w If true, width is returned. Otherwise returns height.
 * @param alias ResourceManager alias
 * @return image width or height.
 */
Vec2i ResourceManager::getImageDimensions(const std::string &alias){
	glImageIt it = _glImages.find(alias);
	
	if(it == _glImages.end()){
		return 0;
	}
	
	return it->second.dimensions();
}

const UV4* ResourceManager::getImageUV(const std::string &alias, int id){
	glImageIt it = _glImages.find(alias);
	
	return (it == _glImages.end())?_glImages["blank"].uv(0):_glImages[alias].uv(id);
}

/**
 * Returns sprite id for given animation and offset
 * 
 * @param alias ResourceManager alias
 * @param testEnum Enum of the animation
 * @param idOffset Offset from the animation start
 * @return id
 */
int ResourceManager::getSpriteId(const std::string &alias, ResourceManager::animation aniEnum, int idOffset){
	return _glSpritesData[alias].id(aniEnum,idOffset);
}

const ResourceManager::fontData* ResourceManager::getFontData(const std::string &alias){
	return &(_glFontsData[alias]);
}

int ResourceManager::getFontKerning(const std::string &alias){
	return _glFontsData[alias].kerning;
}

int ResourceManager::getFontWidth(const std::string &alias){
	return _glFontsData[alias].letterDimensions.x;
}

int ResourceManager::getFontHeight(const std::string &alias){
	return _glFontsData[alias].letterDimensions.y;
}

ResourceManager::imageData::imageData(GLuint glTextureId, const Vec2i& size, const Vec2i& tileSize) : _glTextureId(glTextureId), _size(size), _tileSize(tileSize) {
	_generateMapUV();
}

ResourceManager::imageData::imageData(): _glTextureId(0), _size(Vec2i(0,0)), _tileSize(Vec2i(0,0)){
}

/**
 * Populates the imageData uvs-vector with uv-coordinates for all the tiles
 */
void ResourceManager::imageData::_generateMapUV(){
	for (int i = 0; i < (_size.x/_tileSize.x)*(_size.y/_tileSize.y); i++) {
		UV4 u(_size.x, _size.y, i, _tileSize.x, _tileSize.y);
		//std::cout << i << ": " << u.u0 << "," << u.v0 << " " << u.u1 << "," << u.v1 << "\n";
		_uvs.push_back(u);
	}
	_uvMax = _uvs.size();
}

/**
 * Returns the image width
 * 
 * @return width
 */
int ResourceManager::imageData::width(){
	return _size.x;
}

/**
 * Returns the image height
 * 
 * @return height
 */
int ResourceManager::imageData::height(){
	return _size.y;
}

Vec2i ResourceManager::imageData::dimensions(){
	return _size;
}

/**
 * Returns the image OpenGL resource handle
 * 
 * @return handle
 */
GLuint ResourceManager::imageData::id(){
	return _glTextureId;
}

/**
 * Frees the texture from GPU
 */
void ResourceManager::imageData::free(){
	glDeleteTextures(1,&(_glTextureId));
}

/**
 * Returns a const pointer to the UV4 storing the requested uv-coordinates.
 * 
 * @return const UV4*
 */
const UV4 * ResourceManager::imageData::uv(int i){
	return &(_uvs[i%_uvMax]);
}

ResourceManager::spriteData::spriteData(){
	this->spriteHeight = 0;
	this->spriteWidth = 0;
}

/**
 * Returns a sprite id of the given animation. Returns 0 (the first sprite) if animation is not found.
 * 
 * @param spriteEnum Enum of the animation
 * @param offset Offset from this animation's beginning. Ex: offset 2 will return the third sprite of this animation.
 * @return tile id
 */
int ResourceManager::spriteData::id(ResourceManager::animation spriteEnum, int offset){
       std::map<ResourceManager::animation, ResourceManager::spriteData::oneAnimation >::iterator it = _animations.find(spriteEnum);

       return (it == _animations.end())? 0 : it->second.ids[offset%(it->second.count)];
}

/**
 * Adds ..
 * 
 * @param aniEnum Enum of the animation to add
 * @param ids Reference of vector containing ids
 */
void ResourceManager::spriteData::add(ResourceManager::animation aniEnum, std::vector<int> &ids){
	_animations[aniEnum].ids = ids;
	_animations[aniEnum].count = ids.size();
}
