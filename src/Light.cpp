#include "Light.h"
#include <GL/glew.h>
#include <stdlib.h>

Light::Light( float x, float y, float z ) {
	_data[0] = x;
	_data[1] = y;
	_data[2] = z;
	_data[3] = 1;

	/* Diffuse */
	_data[4] = rand()%50*0.01+0.5;
	_data[5] = rand()%50*0.01+0.5;
	_data[6] = rand()%50*0.01+0.5;

	/* Att */
	_data[7] = 0.0002;

	/* Spotlight x,y,z,enabled*/
	_data[8] = -1;
	_data[9] = 0;
	_data[10] = -0.25;
	_data[11] = 0;

	/* This light is enabled */
	_data[12] = 1;

	//glEnable(GL_LIGHT0+this->n);
	//glLightfv(GL_LIGHT0+this->n, GL_POSITION, this->pos);
	//glLightfv(GL_LIGHT0+this->n, GL_DIFFUSE, this->diffuse);
	//glLightfv(GL_LIGHT0+this->n, GL_AMBIENT, this->ambient);	
}

Light::~Light(){
}

void Light::setColor( float r, float g, float b) {
	_data[LightAttrs::DIFF_R] = r;
	_data[LightAttrs::DIFF_G] = g;
	_data[LightAttrs::DIFF_B] = b;
}

void Light::setPosition( float x, float y ){
	_data[0] = x;
	_data[1] = y;
}

void Light::setPosition( const Vec2f &pos ){
	_data[0] = pos.x;
	_data[1] = pos.y;
}

Vec2f Light::getPosition(){
	return Vec2f( _data[0], _data[1] );
}

void Light::move( float x, float y ){
	_data[0] += x;
	_data[1] += y;
}

void Light::setDirection( float x, float y, float z ){
	_data[8] = x;
	_data[9] = y;
	_data[10] = z;
}

void Light::setOmni( bool omni ){
	_data[11] = (omni == true)? 0.0 : 1.0;
}

void Light::setAtt( float att ){
	_data[7] = att;
}

const float* Light::getShaderData(){
	return _data;
}

void Light::disable(){
	_data[12] = 0;
}

void Light::enable(){
	_data[12] = 1;
}

bool Light::isEnabled(){
	return _data[12] == 1;
}
