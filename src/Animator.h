#ifndef ANIMATOR_H
#define	ANIMATOR_H

#include "ResourceManager.h"

class Animator {
public:
							Animator();
	virtual					~Animator();
							
	bool					animate( const Vec2f &pos );
	ResourceManager::animation getSpriteEnum();
	int						getAnimationId();
	
private:
	Vec2f					_lastPos;
	int						_walkId;
	ResourceManager::animation _aniEnum;
	
	int						_findClosestSpriteRotation( float angle );
};

#endif	/* ANIMATOR_H */
