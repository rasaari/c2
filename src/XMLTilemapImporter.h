#ifndef XMLTILEMAPIMPORTER_H
#define	XMLTILEMAPIMPORTER_H

#include <string>
#include <sstream>
#include "external/tinyxml2/tinyxml2.h"
#include "Tilemap.h"
#include "Scene.h"

using namespace tinyxml2;
using namespace std;

class XMLTilemapImporter {
public:
							XMLTilemapImporter();
	virtual					~XMLTilemapImporter();
	
	bool					import( std::string mapName, Scene& map );
private:
	XMLElement*				_root;
	Tilemap*				_map;
	Scene*					_scene;
	
	void					_readTilesets();
	void					_readTiles();
	void					_readLights();
};

#endif	/* XMLTILEMAPIMPORTER_H */

