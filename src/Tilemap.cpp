#include "Tilemap.h"
#include <cmath>
#include "BinaryIO.h"
#include "ResourceManager.h"
#include "Engine.h"
#include "XMLTilemapImporter.h"

Tilemap::Tilemap(char world, char area){
	world = world;
	area = area;
	_isReady = false;
}

Tilemap::Tilemap() {
	_isReady = false;
}

Tilemap::~Tilemap(){
}


void Tilemap::insertTile( Tile tile ) {
	_isReady = false;
	
	tile.tileSize = tileSize;
	
	if(tile.flags & TILE_WALL){
		tile.setCenter(tile.tileSize);
	}
	
	_tiles.push_back(tile);
}

bool Tilemap::insertTileset( std::string tilesetName ) {
	std::string tilesetPath = tilesetName+".png";
	
	if( ResourceManager::loadTilesetTexture( tilesetName, tilesetPath ) == false ){
		//Tileset could not be loaded
		return false;
	}

	_tilesets.push_back(tilesetName);
	return true;
}

/**
 * Adds a single Tile to the Tilemap to be rendered
 * 
 * @param x Tile coordinates in x-axis
 * @param y Tile coordinates in y-axis
 * @param z Tile layer
 * @param id Tile tileset id
 * @param tileset Tile tileset
 */
void Tilemap::_insert( int x, int y, char layer, int id, const std::string &tileset, int flags ) {
	_isReady = false;
	
	Tile newTile;
	newTile.setPosition(x,y);
	newTile.layer = layer;
	newTile.tileId = id;
	newTile.tileset = tileset;
	newTile.flags = flags;
	newTile.tileSize = 48;

	if(newTile.flags & TILE_WALL){
		newTile.setCenter(this->tileSize);
	}
	
	_tiles.push_back(newTile);
	
	// Clone every tile 10 times to right for testing purposes
	/*
	for( int i = 1; i<10; i++ ){
		newTile.setPosition(x+48*31*i,y);
		if(newTile.flags & TILE_WALL){
			newTile.setCenter(this->tileSize);
		}
		_tiles.push_back(newTile);
	}
	*/
}

/**
 * Sorts the Tilemap::tiles by tileset.
 */
void Tilemap::_sortTiles(){
	std::map<std::string,TilemapBatch> tilemapData;
	for(int i=0; i<_tilesets.size();i++){
		Tilemap::TilemapBatch batch;
		batch.id = i;
		tilemapData.insert(std::pair<std::string,Tilemap::TilemapBatch>(_tilesets.at(i),batch));
	}
	TileSortStruct sort(&tilemapData);
	std::sort(_tiles.begin(), _tiles.end(), sort);
}

void Tilemap::_generateRenderable(){
	if(_tiles.empty())
		return;
	
	_sortTiles();
	_shiftWallCenters();
	_createFloorMap();
	
	Engine::clearCurrentTilemap();
	Engine::generateCurrentTilemap( _tiles );
	
	std::cout << "Generated renderable for tilemap with " << _tiles.size() << " tiles." << std::endl;
	_isReady = true;
}

void Tilemap::_shiftWallCenters(){
	std::vector<Tile>::iterator wIt;
	
	for(std::vector<Tile>::iterator it = _tiles.begin(); it != _tiles.end(); it++){
		if(!(it->flags & TILE_WALL)){
			/* This tile is not a wall */
			continue;
		}
		
		Vec2f pos = it->getPosition();
		pos.y + this->tileSize;
		
		while((wIt = _findLowerWall(pos)) != _tiles.end()){
			// A tile below this is also a wall. 
			it->center.x = wIt->center.x;
			it->center.y = wIt->center.y;
			pos.y += this->tileSize;
		}
	}
}

std::vector<Tile>::iterator Tilemap::_findLowerWall(const Vec2f &pos){
	for(std::vector<Tile>::iterator it = _tiles.begin(); it != _tiles.end(); it++){
		if(it->flags & TILE_WALL && pos == it->getPosition()){
			return it;
		}
	}
	
	/* No tile was found */
	return _tiles.end();
}

/**
 * Renders the Tilemap.
 */
void Tilemap::render(){
	/*
	// Experimental rendering by drawTile
	for(int i = 0; i < this->tiles.size(); i++) {
		if(Engine::isPointVisible(this->tiles[i].getPosition()) || Engine::isPointVisible(Vec2f(48,48) + this->tiles[i].getPosition())){
			Engine::drawTile(&(this->tiles[i]));
		}
	}
	*/
	
	if(!_isReady){
		_generateRenderable();
	}
	
	if(_isReady){
		Engine::drawTilemap();
	}
}

/**
 * Import binary tilemap.
 * DEPRECATED! Maps in this format will not be created
 * anymore
 * 
 * @param name
 * @return success
 */
bool Tilemap::importC2Map( std::string name ) {
	BinaryIO bin;

	this->clear();
	
	//path to map file
	std::string path = "./data/maps/"+name+".c2map";

	//read mapfile into memory
	if(bin.read(path) == false){
		/* File was not found or IO error */
		return false;
	}

	// -- map header --
	this->majorVersion = bin.nextChar();
	this->minorVersion = bin.nextChar();
	
	if(this->majorVersion != MAJOR_VERSION || this->minorVersion != MINOR_VERSION)
		return false;
	
	this->tileSize = bin.nextChar();
	
	mapName = bin.nextString();
	
	//placeholder
	std::string tileset;
	std::string tilesetPath;
	//read tilesets from mapfile
	while((tileset = bin.nextString()).length() > 2) {
		if( !insertTileset( tileset )) {
			return false;
		}
	}
	
	int tilesInSets = bin.nextInt();
	std::vector<char> tilesProperties;
	for(int i = 0; i < tilesInSets; i++) {
		tilesProperties.push_back(bin.nextChar());
	}
	
	std::vector<int> sizes;
	_countTilesetDimensions(&sizes);
	
	// -- map body --
	char cmd, layer;
	int x, y, flags;
	short id;
	std::string tilesetName;
	
	//loop through the rest of the file and try to find
	//map creation instructions. Size of one instruction is a byte
	while(!bin.EndOfFile) {
		cmd = bin.nextChar();

		//new tile
		if(cmd == 0x01) {
			x = bin.nextInt();
			y = bin.nextInt();
			id = bin.nextShort();
			flags = (int)tilesProperties.at(id);
			
			//tile offset
			int offset = 0;
			
			//figure out in which tileset the current tile belongs to
			for(int i = 0; i < _tilesets.size(); i++) {
				if(id >= offset && id < offset + sizes.at(i)) {
					tilesetName = _tilesets.at(i);
					break;
				}
				
				offset += sizes[i];
			}
			id -= offset;

			//add tile to map
			_insert(x, y, layer, id, tilesetName, flags);
		}
		
		//change layer
		else if(cmd == 0x02) {
			layer = bin.nextChar();
		}

		else {
			std::cerr << "unknown instruction! " << cmd;
		}
	}
	return true;
}

void Tilemap::_countTilesetDimensions(std::vector<int>* sizes) {
	for(int i = 0; i < _tilesets.size(); i++) {
		int w = 0, h = 0;
		
		if(ResourceManager::getImage(_tilesets.at(i))) {
			w = ResourceManager::getImageDimensions(_tilesets.at(i)).x;
			h = ResourceManager::getImageDimensions(_tilesets.at(i)).y;

			if(w % this->tileSize != 0 || h % this->tileSize != 0) {
				sizes->push_back(0);
				printf("invalid image size. w = %d (%d) , h = %d (%d)\n", w, w%this->tileSize, h, h%this->tileSize);
			}
			else {
				w = w / this->tileSize;
				h = h / this->tileSize;
			
				sizes->push_back(w*h);
				printf("counted successfully. %d\n", w*h);
			}
		}
		else {
			std::cerr << "No image: " << _tilesets.at(i).data() << "\n";
			sizes->push_back(0);
		}
	}
}

Tile* Tilemap::getTileAt(const Vec2f &pos, float layer) {
	for(int i = 0; i < _tiles.size(); i++) {
		Vec2f fmodValues(std::fmod(pos.x, 48),std::fmod(pos.y, 48));
		
		if(fmodValues.x < 0){
			fmodValues.x += 48;
		}
		
		if(fmodValues.y < 0){
			fmodValues.y += 48;
		}
		
		Tile * t = &_tiles.at(i);
		
		if((pos-fmodValues) == t->getPosition() && t->layer == layer) {
			return t;
		}
	}
	
	return 0;
}

void Tilemap::_createFloorMap(){
	_floorData.clear();
	
	for(std::vector<Tile>::iterator it = _tiles.begin(); it != _tiles.end(); it++){
		if(it->flags & TILE_FLOOR){
			if(_floorData.find(it->getPosition()) == _floorData.end()){
				_floorData.insert(std::pair<Vec2f,bool>(it->getPosition(),true));
			}
		}
		else if(it->flags & TILE_WALL){
			_floorData[it->getPosition()] = false;
		}
	}
}

bool Tilemap::findFloorAt(const Vec2f &pos) const{
	Vec2f lookup(pos.toGrid(48));
	
	floorIt fIt;
	
	if((fIt = _floorData.find(lookup)) != _floorData.end()){
		return fIt->second;
	}
	
	return false;
}

Tilemap::TileSortStruct::TileSortStruct(std::map<std::string, TilemapBatch>* p) : vec(p) {
}

bool Tilemap::TileSortStruct::operator ()(const Tile& a, const Tile& b){
	return vec->at(a.tileset).id < vec->at(b.tileset).id;
}

void Tilemap::clear() {
	_tilesets.clear();
	_tiles.clear();
	_floorData.clear();
	
	_isReady = false;
}