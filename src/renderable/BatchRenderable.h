#ifndef BATCHRENDERABLE_H
#define	BATCHRENDERABLE_H

#include "Renderable.h"

class BatchRenderable : public Renderable{
public:
							BatchRenderable(Shader *shader, int floatsInVertice, GLenum primitiveType);
	virtual					~BatchRenderable();
	
	void					render();

protected:
	bool					updateBatches(const std::string &tileset);
	int						currentBatchId();
	int						closeCurrentBatch();
	void					resetBatches();
	void					addBatch(int begin);
	virtual bool			preRender();
	virtual bool			postRender();
	
private:
	class RenderBatch{
	public:
		std::vector<std::string> _tilesets;	// List of tilesets this batch uses
		
								RenderBatch(BatchRenderable& parent, int begin);

		bool					isCurrent(const std::string &tileset);
		int						close(int currentVerticeCount);
		void					addTileset(const std::string &tileset);
		unsigned int			tilesetCount();
		void					render();
		
	private:
		int						_begin;	// Position of the first vertex of this batch in VBO
		int						_size;	// Number of vertices in this batch
		BatchRenderable&		_parent;
	};
	
	std::vector<RenderBatch>	_renderBatches;
};

#endif	/* BATCHRENDERABLE_H */

