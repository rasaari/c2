#include "TextPainter.h"
#include <string.h>

TextPainter::TextPainter(Shader* shader, int floatsInVertice, GLenum primitiveType) : Renderable(shader, floatsInVertice, primitiveType) {
	ResourceManager::loadFontTexture("Terminus", "Terminus.png", -3);
	fontList["Terminus"] = 0;
	ResourceManager::loadFontTexture("Bisasam", "Bisasam.png", -2);
	fontList["Bisasam"] = 1;
	ResourceManager::loadFontTexture("Nagidal", "Nagidal.png", -7);
	fontList["Nagidal"] = 2;

	_currentFont = "Terminus";

	int maxTiles = 10000;
	int floatsTotal = maxTiles * this->floatsInVertice * this->verticeInPrimitive;

	this->VBO.reserve(floatsTotal);
	this->buildVBO(floatsTotal, GL_STREAM_DRAW);
}

TextPainter::~TextPainter() {
}

void TextPainter::render(){
	_restructureVBO();
	if(!this->shader->isReady() || this->VBO.empty())
		return;
	
	this->updateVBO();
	
	glPushMatrix();
	glTranslatef(0,0,20);
	
	this->shader->enable();
	glBindBuffer(GL_ARRAY_BUFFER, this->VBOid);
	
	this->shader->setVertexPointers();
	
	for(fontIt it = this->fontList.begin(); it != this->fontList.end(); it++){
		glActiveTexture(GL_TEXTURE0+it->second);
		glBindTexture(GL_TEXTURE_2D, ResourceManager::getImage(it->first));
	}

	glDrawArrays(this->primitiveType, 0, this->VBO.size()/this->floatsInVertice);
	
	for(fontIt it = this->fontList.begin(); it != this->fontList.end(); it++){
		glActiveTexture(GL_TEXTURE0+it->second);
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	this->shader->disable();
	glPopMatrix();
}

void TextPainter::drawString(int x, int y, const char *str){
	HandleKey key(x,y,str);
	handleIt it = _bufferHandles.find(key);
	if(it != _bufferHandles.end()){
		/* This string is already in the buffer, so mark it to be kept */
		it->second.keep = 1;
		
		return;
	}

	/* This string is new, so add it to the buffer */
	int length = strlen(str);
	int off = 0;
	const ResourceManager::fontData *data = ResourceManager::getFontData(_currentFont);
	int texId = fontList[_currentFont];
	
	Handle stringHandle;
	stringHandle.first = this->VBO.size();
	stringHandle.last = stringHandle.first+length*20;
	stringHandle.keep = 1;
	
	//bufferHandles[key] = stringHandle;
	_bufferHandles.insert(std::pair<HandleKey,Handle>(key,stringHandle));
	for (int i = 0; i < length; i++) {
		//std::cout << str[i] << " " << (int)str[i] << "\n";
		const UV4 *uv = ResourceManager::getImageUV(_currentFont, (int)str[i]);
		
		this->VBO.push_back(x+off);
		this->VBO.push_back(y);
		this->VBO.push_back(uv->u0);
		this->VBO.push_back(uv->v0);
		this->VBO.push_back(texId);
		
		this->VBO.push_back(x+off);
		this->VBO.push_back(y+data->letterDimensions.x);
		this->VBO.push_back(uv->u0);
		this->VBO.push_back(uv->v1);
		this->VBO.push_back(texId);
		
		this->VBO.push_back(x+data->letterDimensions.x+off);
		this->VBO.push_back(y+data->letterDimensions.y);
		this->VBO.push_back(uv->u1);
		this->VBO.push_back(uv->v1);
		this->VBO.push_back(texId);
		
		this->VBO.push_back(x+data->letterDimensions.x+off);
		this->VBO.push_back(y);
		this->VBO.push_back(uv->u1);
		this->VBO.push_back(uv->v0);
		this->VBO.push_back(texId);
		
		off += data->kerning;
	}
	

}

void TextPainter::setFont(const std::string &alias){
	fontIt it = fontList.find(alias);
	
	if(it != fontList.end()){
		_currentFont = alias;
	}
}

void TextPainter::_restructureVBO(){
	if(_bufferHandles.empty())
		return;
	
	std::vector<Handle*> handles;
	handleIt it = _bufferHandles.begin();
	while(it != _bufferHandles.end()){
		handles.push_back(&(it->second));
		//std::cout << it->second.first << " " << it->second.last << "\n";
		++it;
	}

	std::sort(handles.begin(),handles.end(),TextPainter::_sorter);
	
	int rmOffset = 0;
	std::vector<float>::iterator it1;
	it1 = this->VBO.begin();
	for (int i = 0; i < handles.size(); i++) {
		//std::cout << handles[i]->first << " " << handles[i]->last << "\n";
		handles[i]->first -= rmOffset;
		handles[i]->last -= rmOffset;
		
		if(handles[i]->keep == 0){
			rmOffset += handles[i]->last-handles[i]->first;
			this->VBO.erase(this->VBO.begin()+handles[i]->first,this->VBO.begin()+handles[i]->last);
			it1 = this->VBO.begin();
		}
	}
	it = _bufferHandles.begin();
	while(it != _bufferHandles.end()){
		if(it->second.keep == 0){
			//std::cout << "'" << it->first.str << "' cleared\n";
			_bufferHandles.erase(it++);
		}
		else{
			it->second.keep = 0;
			++it;
		}
	}

}

TextPainter::Handle::Handle() : first(0), last(0), keep(0) {
}

bool TextPainter::Handle::operator ()(const Handle* one, const Handle* other) const{
	return one->first < other->first;
}

TextPainter::HandleKey::HandleKey(int x, int y, const char* str) : x(x),y(y){
	this->str.assign(str);
}

TextPainter::HandleKey::HandleKey() : x(0), y(0), str(" "){
}

bool TextPainter::HandleKey::operator ()(const HandleKey& first, const HandleKey& second) const{
	return (first.x < second.x )|| (first.x == second.x && first.y < second.y) || (first.x == second.x && first.y == second.y && first.str.compare(second.str) < 0);
}

bool TextPainter::_sorter(const Handle* one, const Handle* other) {
	return one->first < other->first;
}

bool TextPainter::_preRender(){
	return false;
}

bool TextPainter::_postRender(){
	return true;
}
