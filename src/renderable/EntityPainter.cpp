#include "EntityPainter.h"
#include "../object/NPC.h"
#include "../object/Tile.h"
#include "../ResourceManager.h"

EntityPainter::EntityPainter(Shader* shader, int floatsInVertice, GLenum primitiveType) : BatchRenderable(shader, floatsInVertice, primitiveType) {
	_maxEntities = 10001;
	int floatsTotal = _maxEntities * this->floatsInVertice * this->verticeInPrimitive;

	this->VBO.reserve(floatsTotal);
	this->buildVBO(floatsTotal, GL_STREAM_DRAW);
}

EntityPainter::~EntityPainter() {
}

void EntityPainter::npc(const NPC *entity){
	if(entity->tileset.empty())
		return;

	this->updateBatches(entity->tileset);

	const UV4 *uv = ResourceManager::getImageUV(entity->tileset, entity->tileId);
	unsigned int id = this->currentBatchId();
	
	Vec2f pos = entity->getPosition();
	float layerOrderFix = entity->layer+(pos.y*0.0001);
	float centerX=0,centerY=0,centerZ=0;
	

	this->VBO.push_back(pos.x);
	this->VBO.push_back(pos.y);
	this->VBO.push_back(layerOrderFix);
	this->VBO.push_back(uv->u0);
	this->VBO.push_back(uv->v1);
	this->VBO.push_back(centerX);
	this->VBO.push_back(centerY);
	this->VBO.push_back(centerZ);
	this->VBO.push_back(id);
	
	this->VBO.push_back(pos.x + entity->size.x);
	this->VBO.push_back(pos.y);
	this->VBO.push_back(layerOrderFix);
	this->VBO.push_back(uv->u1);
	this->VBO.push_back(uv->v1);
	this->VBO.push_back(centerX);
	this->VBO.push_back(centerY);
	this->VBO.push_back(centerZ);
	this->VBO.push_back(id);
	
	this->VBO.push_back(pos.x + entity->size.x);
	this->VBO.push_back(pos.y - entity->size.y);
	this->VBO.push_back(layerOrderFix);
	this->VBO.push_back(uv->u1);
	this->VBO.push_back(uv->v0);
	this->VBO.push_back(centerX);
	this->VBO.push_back(centerY);
	this->VBO.push_back(centerZ);
	this->VBO.push_back(id);
	
	this->VBO.push_back(pos.x);
	this->VBO.push_back(pos.y - entity->size.y);
	this->VBO.push_back(layerOrderFix);
	this->VBO.push_back(uv->u0);
	this->VBO.push_back(uv->v0);
	this->VBO.push_back(centerX);
	this->VBO.push_back(centerY);
	this->VBO.push_back(centerZ);
	this->VBO.push_back(id);

}

void EntityPainter::tile(const Tile *entity){
	if(entity->tileset.empty())
		return;
	
	this->updateBatches(entity->tileset);
	
	const UV4 *uv = ResourceManager::getImageUV(entity->tileset, entity->tileId);
	unsigned int id = this->currentBatchId();

	Vec2f pos = entity->getPosition();

	this->VBO.push_back(pos.x);
	this->VBO.push_back(pos.y);
	this->VBO.push_back(entity->layer);
	this->VBO.push_back(uv->u0);
	this->VBO.push_back(uv->v0);
	this->VBO.push_back(entity->center.x);
	this->VBO.push_back(entity->center.y);
	this->VBO.push_back(entity->center.z);
	this->VBO.push_back(id);
	
	this->VBO.push_back(pos.x);
	this->VBO.push_back(pos.y+entity->tileSize);
	this->VBO.push_back(entity->layer);
	this->VBO.push_back(uv->u0);
	this->VBO.push_back(uv->v1);
	this->VBO.push_back(entity->center.x);
	this->VBO.push_back(entity->center.y);
	this->VBO.push_back(entity->center.z);
	this->VBO.push_back(id);
	
	this->VBO.push_back(pos.x+entity->tileSize);
	this->VBO.push_back(pos.y+entity->tileSize);
	this->VBO.push_back(entity->layer);
	this->VBO.push_back(uv->u1);
	this->VBO.push_back(uv->v1);
	this->VBO.push_back(entity->center.x);
	this->VBO.push_back(entity->center.y);
	this->VBO.push_back(entity->center.z);
	this->VBO.push_back(id);
	
	this->VBO.push_back(pos.x+entity->tileSize);
	this->VBO.push_back(pos.y);
	this->VBO.push_back(entity->layer);
	this->VBO.push_back(uv->u1);
	this->VBO.push_back(uv->v0);
	this->VBO.push_back(entity->center.x);
	this->VBO.push_back(entity->center.y);
	this->VBO.push_back(entity->center.z);
	this->VBO.push_back(id);
}

bool EntityPainter::preRender(){
	if(!BatchRenderable::preRender()){
		return false;
	}
	
	this->closeCurrentBatch();
	this->updateVBO();
	return true;
}

bool EntityPainter::postRender(){
	this->VBO.clear();
	this->resetBatches();
	return true;
}