/* 
 * File:   TilemapPainter.cpp
 * Author: knasaari
 * 
 * Created on 31. elokuuta 2013, 17:34
 */

#include "TilemapPainter.h"
#include "../object/Tile.h"
#include "../ResourceManager.h"

TilemapPainter::TilemapPainter(Shader* shader, int floatsInVertice, GLenum primitiveType) : BatchRenderable(shader, floatsInVertice, primitiveType) {
}

TilemapPainter::~TilemapPainter() {
}

void TilemapPainter::generate(const std::vector<Tile> &tiles){
	this->clear();
	
	int size = tiles.size();
	Vec2f pos;
	bool tilesetChanged = false;
	int currentTextureId = 0;
	
	for (int i = 0; i < size; i++) {
		tilesetChanged = this->updateBatches(tiles[i].tileset);
		
		if(tilesetChanged){
			currentTextureId = this->currentBatchId();
		}
		
		const UV4 *uv = ResourceManager::getImageUV(tiles[i].tileset, tiles[i].tileId);
		pos = tiles[i].getPosition();
		float layerOrderFix = (tiles[i].layer == 3)?tiles[i].layer+(pos.y*0.0001)+(24*3*0.0001):tiles[i].layer;
		
		this->VBO.push_back(pos.x);
		this->VBO.push_back(pos.y);
		this->VBO.push_back(layerOrderFix);
		this->VBO.push_back(uv->u0);
		this->VBO.push_back(uv->v0);
		this->VBO.push_back(tiles[i].center.x);
		this->VBO.push_back(tiles[i].center.y);
		this->VBO.push_back(tiles[i].center.z);
		this->VBO.push_back(currentTextureId);

		this->VBO.push_back(pos.x);
		this->VBO.push_back(pos.y+48);
		this->VBO.push_back(layerOrderFix);
		this->VBO.push_back(uv->u0);
		this->VBO.push_back(uv->v1);
		this->VBO.push_back(tiles[i].center.x);
		this->VBO.push_back(tiles[i].center.y);
		this->VBO.push_back(tiles[i].center.z);
		this->VBO.push_back(currentTextureId);
		
		this->VBO.push_back(pos.x+48);
		this->VBO.push_back(pos.y+48);
		this->VBO.push_back(layerOrderFix);
		this->VBO.push_back(uv->u1);
		this->VBO.push_back(uv->v1);
		this->VBO.push_back(tiles[i].center.x);
		this->VBO.push_back(tiles[i].center.y);
		this->VBO.push_back(tiles[i].center.z);
		this->VBO.push_back(currentTextureId);
		
		this->VBO.push_back(pos.x+48);
		this->VBO.push_back(pos.y+48);
		this->VBO.push_back(layerOrderFix);
		this->VBO.push_back(uv->u1);
		this->VBO.push_back(uv->v1);
		this->VBO.push_back(tiles[i].center.x);
		this->VBO.push_back(tiles[i].center.y);
		this->VBO.push_back(tiles[i].center.z);
		this->VBO.push_back(currentTextureId);
	
		this->VBO.push_back(pos.x+48);
		this->VBO.push_back(pos.y);
		this->VBO.push_back(layerOrderFix);
		this->VBO.push_back(uv->u1);
		this->VBO.push_back(uv->v0);
		this->VBO.push_back(tiles[i].center.x);
		this->VBO.push_back(tiles[i].center.y);
		this->VBO.push_back(tiles[i].center.z);
		this->VBO.push_back(currentTextureId);
		
		this->VBO.push_back(pos.x);
		this->VBO.push_back(pos.y);
		this->VBO.push_back(layerOrderFix);
		this->VBO.push_back(uv->u0);
		this->VBO.push_back(uv->v0);
		this->VBO.push_back(tiles[i].center.x);
		this->VBO.push_back(tiles[i].center.y);
		this->VBO.push_back(tiles[i].center.z);
		this->VBO.push_back(currentTextureId);

	}
	this->closeCurrentBatch();
	
	this->buildVBO(this->VBO.size(), GL_STATIC_DRAW);
	this->updateVBO();
}

void TilemapPainter::clear(){
	this->VBO.clear();
	this->resetBatches();
}

bool TilemapPainter::preRender(){
	if(!BatchRenderable::preRender()){
		return false;
	}
	
	return true;
}
bool TilemapPainter::postRender(){
	return true;
}
