#include "BatchRenderable.h"
#include "../ResourceManager.h"

BatchRenderable::BatchRenderable(Shader* shader, int floatsInVertice, GLenum primitiveType) : Renderable(shader, floatsInVertice, primitiveType) {
	resetBatches();
}

BatchRenderable::~BatchRenderable() {
}

/**
 * Queries the current batch and checks if the current tileset matches with the provided one.
 * Handles adding a new batch if maximum amount of different tilesets in a batch has been reached.
 * 
 * @param tileset
 * @return true if a new tileset was added to the current batch
 */
bool BatchRenderable::updateBatches(const std::string &tileset){
	// Current batch's current tileset didn't match the queried tileset
	if(_renderBatches.back().isCurrent(tileset) == false){ 
		 // Maximum amount of different tilesets in a batch has been reached
		if(_renderBatches.back().tilesetCount() >= this->maxTilesetsInBatch){
			// Add a new batch
			this->addBatch(this->closeCurrentBatch());
		}
		// Add tileset to the current batch
		_renderBatches.back().addTileset(tileset);
		return true;
	}
	return false;
}

int BatchRenderable::closeCurrentBatch(){
	std::vector<RenderBatch>::iterator it = --(_renderBatches.end());
	return it->close(this->VBO.size()/this->floatsInVertice);
}

void BatchRenderable::addBatch(int begin){
	_renderBatches.push_back(RenderBatch(*this,begin));
}

void BatchRenderable::resetBatches(){
	_renderBatches.clear();
	this->addBatch(0);
}

int BatchRenderable::currentBatchId(){
	return _renderBatches.back().tilesetCount()-1;
}

bool BatchRenderable::preRender(){
	return !this->VBO.empty();
}

bool BatchRenderable::postRender(){
	return true;
}

void BatchRenderable::render(){
	if(!this->shader->isReady() || !this->preRender()){
		this->postRender();
		return;
	}
	glPushMatrix();
	this->shader->enable();
	glBindBuffer(GL_ARRAY_BUFFER, this->VBOid);

	this->shader->setVertexPointers();
	
	for (int i = 0; i < _renderBatches.size(); i++){
		_renderBatches[i].render();
	}
	
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	this->shader->disable();
	glPopMatrix();
	
	this->postRender();
}

/*--------- RenderBatch ----------*/

BatchRenderable::RenderBatch::RenderBatch(BatchRenderable& parent, int begin):_parent(parent),_begin(begin),_size(0){
}

bool BatchRenderable::RenderBatch::isCurrent(const std::string& tileset){
	if(_tilesets.empty() || _tilesets.back() != tileset)
		return false;
		
	return true;
}
int BatchRenderable::RenderBatch::close(int currentVerticeCount){
	_size = currentVerticeCount - _begin;
	return _size;
}

void BatchRenderable::RenderBatch::addTileset(const std::string &tileset){
	_tilesets.push_back(tileset);
}

unsigned int BatchRenderable::RenderBatch::tilesetCount(){
	return _tilesets.size();
}

void BatchRenderable::RenderBatch::render(){
	int size = _tilesets.size();
	
	// Bind batch tilesets
	for (int tSet = 0; tSet < size; tSet++) {
		glActiveTexture(GL_TEXTURE0+tSet);
		glBindTexture(GL_TEXTURE_2D, ResourceManager::getImage(_tilesets[tSet]));
	}
	
	glDrawArrays(_parent.primitiveType, _begin, _size);
	
	// Free batch tilesets
	for (int tSet = 0; tSet < size; tSet++) {
		glActiveTexture(GL_TEXTURE0+tSet);
		glBindTexture(GL_TEXTURE_2D, 0); 
	}
}