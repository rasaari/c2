/* 
 * File:   TilemapPainter.h
 * Author: knasaari
 *
 * Created on 31. elokuuta 2013, 17:34
 */

#ifndef TILEMAPPAINTER_H
#define	TILEMAPPAINTER_H

#include "BatchRenderable.h"
class Tile;

class TilemapPainter : public BatchRenderable{
public:
							TilemapPainter(Shader *shader, int floatsInVertice, GLenum primitiveType);
	virtual					~TilemapPainter();
	
	void					generate(const std::vector<Tile> &tiles);
	void					clear();
private:
	bool					preRender();
	bool					postRender();
};

#endif	/* TILEMAPPAINTER_H */

