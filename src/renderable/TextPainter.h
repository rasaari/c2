/* 
 * File:   TextPainter.h
 * Author: knasaari
 *
 * Created on 31. elokuuta 2013, 17:50
 */

#ifndef TEXTPAINTER_H
#define	TEXTPAINTER_H

#include "Renderable.h"
#include "../ResourceManager.h"
#include <algorithm>
#include <map>

class TextPainter : public Renderable {
public:
							TextPainter(Shader *shader, int floatsInVertice, GLenum primitiveType);
	virtual					~TextPainter();
	
	void					render();
	void					drawString(int x, int y, const char *str);
	void					setFont(const std::string &alias);
private:
	std::string				_currentFont;
	std::map<std::string,int> fontList;
	typedef std::map<std::string,int>::iterator fontIt;
	
	struct Handle{
		int first,last,keep;
		
		Handle();
		
		bool operator()(const Handle *one, const Handle *other) const;
	};
	
	struct HandleKey{
		int x,y;
		std::string str;
		
		HandleKey(int x, int y, const char *str);
		HandleKey();
		
		bool operator()(const HandleKey &first, const HandleKey &second) const;
	};
	
	std::map<HandleKey,Handle,HandleKey> _bufferHandles;
	typedef std::map<HandleKey,Handle,HandleKey>::iterator handleIt;
	
	bool					_preRender();
	bool					_postRender();
	void					_restructureVBO();
	static bool				_sorter(const Handle *one, const Handle *other);

};

#endif	/* TEXTPAINTER_H */

