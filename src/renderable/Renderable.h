#ifndef RENDERABLE_H
#define	RENDERABLE_H

#include <GL/glew.h>
#include <vector>
#include <string>
#include "../shader/Shader.h"
/**
 * This is the base class for everything that does rendering.
 */
class Renderable {
public:
						Renderable(Shader *shader, int floatsInVertice, GLenum primitiveType);
	virtual					~Renderable();
	
	virtual void				render();
	void					setShader(Shader* shader);
protected:
	const GLenum				primitiveType;
	const int				floatsInVertice;
	const int				verticeInPrimitive;
	int					maxTilesetsInBatch;
	GLuint					VBOid;
	std::vector<float>			VBO;
	Shader*					shader;
	
	void					buildVBO(int numFloats, GLenum usage);
	void					updateVBO();
};

#endif	/* RENDERABLE_H */

