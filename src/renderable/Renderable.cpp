#include "Renderable.h"
#include "../ResourceManager.h"
//Shader * Renderable::shader;

Renderable::Renderable(Shader* shader, int floatsInVertice, GLenum primitiveType) : shader(shader), floatsInVertice(floatsInVertice), primitiveType(primitiveType), verticeInPrimitive(((primitiveType == GL_TRIANGLES) ? 6 : 4)) {
	VBOid = 0;
	maxTilesetsInBatch = shader->getMaxTextures();
	this->shader = shader;
	glGenBuffers(1, &(this->VBOid));
}

Renderable::~Renderable() {
	glDeleteBuffers(1,&(this->VBOid));
}

void Renderable::buildVBO(int numFloats, GLenum usage){
	int size = numFloats*sizeof(GLfloat);

	glBindBuffer(GL_ARRAY_BUFFER, this->VBOid);
	glBufferData(GL_ARRAY_BUFFER, size, NULL, usage);
	glBindBuffer(GL_ARRAY_BUFFER,0);
}

void Renderable::updateVBO(){
	glBindBuffer(GL_ARRAY_BUFFER, this->VBOid);
	glBufferSubData(GL_ARRAY_BUFFER, 0, this->VBO.size()*sizeof(GLfloat), &(this->VBO[0]));
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Renderable::render(){
	/*
	if(!this->shader->isReady() || !this->preRender()){
		this->postRender();
		return;
	}
	glPushMatrix();
	this->shader->enable();
	glBindBuffer(GL_ARRAY_BUFFER, this->VBOid);

	this->shader->setVertexPointers();
	for (int i = 0; i < this->renderBatches.size(); i++){
		int size = this->renderBatches[i].tilesets.size();
		for (int tSet = 0; tSet < size; tSet++) {
			glActiveTexture(GL_TEXTURE0+tSet);
			glBindTexture(GL_TEXTURE_2D, ResourceManager::getImage(this->renderBatches[i].tilesets[tSet]));
			std::cout << tSet << " " << this->renderBatches[i].tilesets[tSet] << "\n";
		}

		glDrawArrays(this->primitiveType, this->renderBatches[i].begin, this->renderBatches[i].size);
		std::cout << this->renderBatches[i].begin << " " << this->renderBatches[i].size << "\n";
		for (int tSet = 0; tSet < size; tSet++) {
			glActiveTexture(GL_TEXTURE0+tSet);
			glBindTexture(GL_TEXTURE_2D, 0); 
		}
	}
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	this->shader->disable();
	glPopMatrix();
	
	this->postRender();
	*/
}

void Renderable::setShader(Shader* shader){
	this->shader = shader;
}
