#ifndef PAINTER_H
#define	PAINTER_H

#include "BatchRenderable.h"
class NPC;
class Tile;

class EntityPainter : public BatchRenderable{
public:
							EntityPainter(Shader *shader, int floatsInVertice, GLenum primitiveType);
	virtual					~EntityPainter();
	
	void					npc(const NPC *entity);
	void					tile(const Tile *entity);
private:
	int _maxEntities;
	
	bool					preRender();
	bool					postRender();
};

#endif	/* PAINTER_H */

