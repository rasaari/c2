#ifndef LIGHT_H
#define	LIGHT_H

#include "Utility.h"

enum LightAttrs {
	X = 0,
	Y,
	Z,
	VALUE, // ?
	DIFF_R,
	DIFF_G,
	DIFF_B,
	ATT, // ?
	SPOT_X,
	SPOT_Y,
	SPOT_Z,
	SPOT_ENABLED,
	ENABLED
};

class Light {
public:

							Light( float x, float y, float z );
							Light( Vec3f position ) : Light( position.x, position.y, position.z){};
	virtual					~Light();
	
	void					setColor( float r, float g, float b);
	void					setPosition( float x, float y );
	void					setPosition( const Vec2f &pos );
	Vec2f					getPosition();
	void					move( float x, float y );
	void					setDirection( float x, float y, float z );
	void					setOmni( bool omni );
	void					setAtt( float att );

	const float*			getShaderData();
	void					disable();
	void					enable();
	bool					isEnabled();

private:
	float					_data[13];
};

#endif	/* LIGHT_H */

