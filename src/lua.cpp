/*
#include "lua.h"
#include <sstream>
#include "object/NPC.h"
#include "Console.h"
#include "GameStateManager.h"
#include "gameStates/Game.h"

lua_State * luaState;

namespace lua {
	bool init() {
		luaState = luaL_newstate();

		luabind::open(luaState);
		luaL_openlibs(luaState);

		luabind::module(luaState) [
			luabind::def("addDude", lua::addDude),
			luabind::def("echo", lua::echo),
			luabind::def("setPlayerPosition", lua::setPlayerPosition),
			luabind::def("help", lua::help),
			luabind::def("loadMap", lua::loadMap),
			luabind::class_<NPC>("NPC")
				.def(luabind::constructor<int, int, std::string>())
				.def("create", &NPC::create)
		];
	
		return true;
	}
	
	void addDude(int x, int y) {
		GameState_Game *gs = (GameState_Game*)GameStateManager::getState();

		NPC * testNPC = new NPC(48,96,"cool");
		testNPC->setPosition(x,y);

		gs->scene.insert(testNPC);
		gs->dudes.push_back(testNPC);

		std::stringstream response;
		response << "new npc @(" << x << ", " << y << "), id " << testNPC->getId(); 
		Console::log(response.str());
	}

	void setPlayerPosition(int x, int y) {
		GameState_Game *gs = (GameState_Game*)GameStateManager::getState();
		NPC* player = gs->getPlayer();
		player->setPosition(x,y);


		std::stringstream response;
		response << "player positioned to (" << x << ", " << y << ")";

		Console::log(response.str());
	}
	
	void loadMap(std::string mapName) {
		GameState_Game *gs = (GameState_Game*)GameStateManager::getState();
		gs->loadMap(mapName);
	}
	
	void echo(std::string line) {
		Console::log(line);
	}
	
	void help() {
		Console::log("----");
		Console::log("This is a lua console with following additions:");
		Console::log("addNPC(x ,y)");
		Console::log("echo(\"string\")");
		Console::log("setPlayerPosition(x, y)");
		Console::log("help()");
		Console::log("----");
	}
}
 */