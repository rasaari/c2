/* 
 * File:   lua.h
 * Author: jari
 *
 * Created on July 23, 2013, 11:22 AM
 */

/*
#ifndef LUA_H
#define	LUA_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "lua5.1/lua.h"
#include "lua5.1/lualib.h"
#include "lua5.1/lauxlib.h"


#ifdef	__cplusplus
}
#endif

#include <luabind/luabind.hpp>

extern lua_State *luaState;

namespace lua {
	bool init();
	void addDude(int, int);
	void setPlayerPosition(int, int);
	void loadMap(std::string mapName);
	
	void echo(std::string);
	void help();
}
#endif	/* LUA_H */