#ifndef _ENGINE_H
#define _ENGINE_H

#define NO_SDL_GLEXT
#include <GL/glew.h>
#include <SDL/SDL.h>
#include <stdio.h>
#include <string>
#include <map>
#include <iostream>
//#include "lua.h"
#include "boost/filesystem.hpp"
#include "boost/filesystem/operations.hpp"
#include "boost/chrono.hpp"
#include "Graphics.h"
#include "Utility.h"

typedef boost::chrono::high_resolution_clock::time_point crTimePoint;
typedef boost::chrono::high_resolution_clock crClock;
typedef boost::chrono::duration<double> crDuration;

class Engine {
public:
	char*					caption;
	int						finalObjectId;
	
							Engine();

	int						start();
	bool					run();
	void					setWindowSize(int width, int height);
	void					testMap(std::string);
	static std::string		getTestMap();
	void					cleanUp();
	void					eventLoop();
	
	static void				setSimpleShader(bool on);
	static void				quit(std::string msg);
	static float			appSpeed();
	static bool				tick();
	static bool				keyHit(SDLKey key);
	static bool				keyDown(SDLKey key);
	static void				setKeyState(int key, int state);
	static void				createKeyDownEvent(int key);
	static int				getNewId();
	
	static void				drawNPC(const NPC *entity);
	static void				drawTile(const Tile *entity);
	static void				drawEntityBatches();
	
	static void				generateCurrentTilemap(const std::vector<Tile> &tiles);
	static void				drawTilemap();
	static void				clearCurrentTilemap();
	
	static void				drawString(float x, float y, const char *str);
	static void				setFont(const std::string &alias);
	void					drawTextBatches();
	
	static void				setCamera(const Vec2f &position);
	static Vec2f				getCamera();
	static Vec2f				getWorldPosition(const Vec2f &screenPosition);
	static void				setLights(const float *lightData, int lightCount);
	static bool				isPointVisible(const Vec2f &pos);
	
	static void				refreshShaders();
	static int				getScreenWidth();
	static int				getScreenHeight();
private:
	//TODO: clean up this horrible mess
	static Engine*			_instance;
	int						_screenWidth, _screenHeight;
	bool					_running, _isTesting, _isDevConsole, _isSimpleShader;
	Uint32					_timer;
	boost::chrono::high_resolution_clock::time_point _lastFrame, _currentFrame;
	boost::chrono::duration<double>		_duration;
	std::string				_testingMap;
	Uint8*					_keyStates;
	Uint8*					_keyStatesLast;
	TileShader*				_tileShader;
	TextShader*				_textShader;
	std::map<SDLKey, bool>	_keyHitStates;
	int						_keyStateSize;
	float					_EappSpeed;
	bool					_Etick;
	Graphics*				_graphics;
	Vec2f					_screenCorners[4];
	SDL_Event				_event;
	double					_averageTime;
	float					_lastAverageFPS;
	int						_averageCount, _average_tickcount;
	
	bool					_init();
	int						_initOpenGL();
	void					_resizeGL();
	void					_update();
	void					_render();
	void					_handleEvent(SDL_Event* Event);
	void					_updateKeyStates();
	void					_updateLastKeyStates();
	void					_fillZeroKeyStates();
	void					_updateAppSpeed();
	void					_updateTick();
	void					_updateCulling();
};

#endif