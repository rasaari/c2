#include "Engine.h"

#include <string.h>
#include <algorithm> 
#include "GameStateManager.h"
#include "ResourceManager.h"
#include "gameStates/Game.h"
#include "Console.h"

Engine *Engine::_instance = NULL;
/**
 * Engine constructor. No resource loading or other stuff here.
 */
Engine::Engine() {
	_instance = this;
	
	_screenWidth = 800;
	_screenHeight = 600;
	
	_EappSpeed = 1;
	_Etick = false;
	
	_running = false;
	_isTesting = false;
	_testingMap = "";
	_isDevConsole = false;
	_isSimpleShader = false;

	Console::log("-- welcome to C2 engine --");
	
	finalObjectId = 1;
	
	_currentFrame = boost::chrono::high_resolution_clock::now();
}

/**
 * Starts the game-engine. Initializes everything, so run() can be called.
 */
int Engine::start() {
	/* Try to initialize everything. */
	if (_init() == false ) return -1;

	SDL_EnableUNICODE( SDL_ENABLE );
	
	ResourceManager::init();
	ResourceManager::loadSpriteTexture( "cool", "stickwalk_8.png" );
	ResourceManager::loadSpriteTexture( "cool2", "cool.png" );
	
	_graphics = new Graphics();
	
	//GameState_Game *gsTest = new GameState_Game();
	//GameStateManager::setState(gsTest);
	
	//if(this->isTesting == true)
	//	gsTest->init(this->testingMap);
	//else
	//	gsTest->init();
	GameStateManager::callInit();
	
	this->_keyStates = SDL_GetKeyState( &(_keyStateSize ) );
	this->_keyStatesLast = (Uint8*)malloc( sizeof( Uint8 )*_keyStateSize);
	this->_updateLastKeyStates();
	
	this->_timer = SDL_GetTicks();
	
	_running = true;
	_averageTime = 0;
	_lastAverageFPS = 0;
	_averageCount = 0;
	_average_tickcount = 0;
	
	return 0;
}

void Engine::eventLoop() {
	/* If there are events in the queue, loop through them all */
	while( SDL_PollEvent( &_event ) ) {
		_handleEvent( &_event );
	}
}

bool Engine::run(){
	if(!_running)
		return false;
	
	_updateAppSpeed();
	_updateTick();
	_updateCulling();
	
	this->eventLoop();
	
	if( _isDevConsole == true ){
		/* developer console is enabled, disable keyHit() and keyDown() */
		_fillZeroKeyStates();
	}

	else{	
		/* update keyHit() and keyDown() */
		_updateKeyStates();
	}

	/* Update logic and render current gameState */
	_update();
	_render();

	/* FPS */
	crDuration time_span = boost::chrono::duration_cast<crDuration>(this->_currentFrame-this->_lastFrame);
	_averageTime += time_span.count();
	_averageCount++;

	if( _Etick == true ){
		_average_tickcount++;
	}

	if( _average_tickcount == 10 ){
		_lastAverageFPS = 1000.0/(_averageTime/_averageCount*1000.0);
		_averageTime = 0;
		_averageCount = 0;
		_average_tickcount = 0;
	}

	char fpsLoL[20];
	sprintf( fpsLoL,"FPS: %.2f", _lastAverageFPS );
	Engine::drawString( 10.0, 10.0, fpsLoL );
	
	return true;
}

void Engine::setWindowSize(int width, int height){
	_screenWidth = (width < 100)?100:width;
	_screenHeight = (height < 100)?100:height;
}

bool Engine::_init() {
	this->caption = (char*)"C2";
	
	if (SDL_Init (SDL_INIT_TIMER | SDL_INIT_VIDEO) < 0 ) {
		return false;
	}
	
	#ifdef __WIN32__
		freopen( "CON", "w", stdout );
		freopen( "CON", "w", stderr );
	#endif
		
	if( _initOpenGL() == 0 ){
		 return false;
	}
		
	srand( time( NULL ) );
	
	GameStateManager::init();
	
	return true;
}

int Engine::_initOpenGL(){
	#ifndef __C2_LIB__
		SDL_GL_SetAttribute( SDL_GL_RED_SIZE, 8 );
		SDL_GL_SetAttribute( SDL_GL_GREEN_SIZE, 8 );
		SDL_GL_SetAttribute( SDL_GL_BLUE_SIZE, 8 );
		SDL_GL_SetAttribute( SDL_GL_ALPHA_SIZE, 8 );

		SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 16 );
		SDL_GL_SetAttribute( SDL_GL_BUFFER_SIZE, 32 );
		SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );


		if((SDL_SetVideoMode( _screenWidth, _screenHeight,32, SDL_OPENGL | SDL_RESIZABLE)) == NULL) {
			std::cerr << "Couldn't set video mode: " << SDL_GetError() << "\n";
			return 0;
		}
	#endif

	GLenum err = glewInit();
	if ( GLEW_OK != err ){
		/* GLEW failed */
		std::cerr << "GLEW failed: " << glewGetErrorString(err);
		return 0;
	}
	
	/* Make sure that OpenGL 2.1 is supported */
	if( glewIsSupported( "GL_VERSION_2_1" ) == false ){
		std::cerr << "Your system does not support OpenGL 2.1, it only supports " << glGetString(GL_VERSION) << ". (is your GPU made of wood?)\n";
		return 0;
	}
	
	/* Display supported OpenGL & GLSL versions */
	std::cout << "OpenGL: " << glGetString(GL_VERSION) << ", GLSL: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << "\n\n";
		
	glDisable( GL_LIGHTING );
	
	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	glDisable( GL_BLEND );

	glEnable( GL_DEPTH_TEST );
	glDepthFunc( GL_LEQUAL );

	glClearColor( 0.0, 0.0, 0.0, 1.0 );
	
	this->_resizeGL();
	
	return 2;
}

void Engine::_resizeGL(){
	glViewport( 0, 0, _screenWidth, _screenHeight );

	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();
	glOrtho( 0, _screenWidth, _screenHeight, 0, 0.1, 32 );
	glMatrixMode( GL_MODELVIEW );

	glLoadIdentity();
}

void Engine::_handleEvent( SDL_Event* Event ) {
	if(Event->type == SDL_QUIT){
		_running = false;
	}
	else if(Event->type == SDL_KEYDOWN && ((int)Event->key.keysym.unicode == 167 || Event->key.keysym.sym == SDLK_TAB)) {
		_isDevConsole = !_isDevConsole;
	}
	else if(Event->type == SDL_USEREVENT){
		/* code 1 = exit the program  */
		if(Event->user.code == 1){
			_running = false;
			std::cout << *(std::string*)(Event->user.data1) << "\n";
		}
		else{
			Console::log("Unknown userevent");
		}
	}
	else if(Event->type == SDL_VIDEORESIZE){
		this->setWindowSize(Event->resize.w, Event->resize.h);
	}
	else if(_isDevConsole) {
		/* If dev console is enabled, divert all other events to Console */
		Console::handleEvent(Event);
	}
	else{
		GameStateManager::handleEvent(Event);
	}
}

void Engine::cleanUp() {
	/* Ask resource manager to free all resources that are loaded by it. */
	ResourceManager::freeImages();

	std::cout << "\n";

	#ifdef __WIN32__
		freopen( "stdout.txt", "w", stdout );
		freopen( "stderr.txt", "w", stderr );
	#endif
}

void Engine::_updateKeyStates(){
	SDL_PumpEvents();
	for (int i = 0; i < _keyStateSize; i++) {
		this->_keyHitStates[(SDLKey)i] = (_keyStates[i] == 1 && _keyStatesLast[i] == 0)? true : false;
	}
	this->_updateLastKeyStates();
}

void Engine::_updateLastKeyStates(){
	std::copy( _keyStates, _keyStates+_keyStateSize, _keyStatesLast );
}

void Engine::_fillZeroKeyStates(){
	std::fill( _keyStates, _keyStates+_keyStateSize, 0 );
}

void Engine::_updateAppSpeed(){
	_lastFrame = _currentFrame;
	_currentFrame = crClock::now();
	_duration = boost::chrono::duration_cast<crDuration>(_currentFrame-_lastFrame);
	_EappSpeed = std::min(3.0,60.0/(1.0/_duration.count()));
}

void Engine::_updateTick(){
	if(_Etick == true){
		_Etick = false;
	}

	// Set tick to true every 1/10th of a second
	if( SDL_GetTicks() >= _timer+100 ){
		_timer = SDL_GetTicks();
		_Etick = true;
	}
}

void Engine::_updateCulling(){
	Vec2f camera = _graphics->getCamera();
	// Top left
	_screenCorners[0].x = -camera.x;
	_screenCorners[0].y = -camera.y;
	
	// Bottom left
	_screenCorners[1].x = -camera.x;
	_screenCorners[1].y = -camera.y + _screenHeight;
	
	// Bottom right
	_screenCorners[2].x = -camera.x + _screenWidth;
	_screenCorners[2].y = -camera.y + _screenHeight;
	
	// Top right
	_screenCorners[3].x = -camera.x + _screenWidth;
	_screenCorners[3].y = -camera.y; 
}

void Engine::_update() {
	//call update method of the active gamestate
	GameStateManager::update();
}

void Engine::_render() {
	// reset & clear the screen
	this->_resizeGL();
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	
	glTranslatef( 0, 0, -30 );
	this->drawTextBatches();
	glLoadIdentity();
	
	_graphics->setSimpleShader(_isSimpleShader);
	
	//call render method of the active game state
	GameStateManager::render();
	
	if( _isDevConsole ) {
		Console::render();
	}
	
	#ifndef __C2_LIB__
		SDL_GL_SwapBuffers();
	#endif
}

void Engine::testMap( std::string map ){
	std::string path = "data/maps/"+map+".c2map";
	std::ifstream ifile( path.c_str() );
	
	if( !ifile ) return;

	_testingMap = map;
}

std::string Engine::getTestMap() {
	return _instance->_testingMap; 
}

void Engine::setSimpleShader( bool enabled ){
	_instance->_isSimpleShader = enabled;
}

void Engine::quit( std::string msg ){
	std::string * message = new std::string( msg );
	SDL_Event event;
	event.type = SDL_USEREVENT;
	event.user.code = 1;
	event.user.data1 = message;
	SDL_PushEvent(&event);
}

float Engine::appSpeed(){
	return _instance->_EappSpeed;
}

bool Engine::tick(){
	return _instance->_Etick;
}

bool Engine::keyHit( SDLKey key ){
	return _instance->_keyHitStates[key];
}

bool Engine::keyDown( SDLKey key ){
	return ( _instance->_keyStates[key] == 1 )? true : false;
}

void Engine::setKeyState( int key, int state ){
	_instance->_keyStates[(SDLKey)key] = state;
}

void Engine::createKeyDownEvent(int key){
	SDL_Event event;
	event.type = SDL_KEYDOWN;

	event.key.keysym.sym = (SDLKey)key;
	event.key.state = SDL_PRESSED;
	event.key.keysym.unicode = (Uint16)key;
	//event.key.type = SDL_KEYDOWN;
	SDL_PushEvent(&event);
}

int Engine::getNewId() {
	return ++( _instance->finalObjectId );
}

void Engine::drawNPC( const NPC *entity ){
	_instance->_graphics->drawNPC( entity );
}

void Engine::drawTile( const Tile *entity ){
	_instance->_graphics->drawTile( entity );
}

void Engine::drawEntityBatches(){
	_instance->_graphics->drawEntityPainter();
}

void Engine::drawString( float x, float y, const char *str ){
	_instance->_graphics->drawString( x, y, str );
}

void Engine::setFont( const std::string &alias ){
	_instance->_graphics->setFont( alias );
}

void Engine::drawTextBatches(){
	_instance->_graphics->drawTextPainter();
}

void Engine::generateCurrentTilemap( const std::vector<Tile> &tiles ){
	_instance->_graphics->setupTileMap( tiles );
}

void Engine::drawTilemap(){
	_instance->_graphics->drawTileMap();
}

void Engine::clearCurrentTilemap(){
	_instance->_graphics->clearTileMap();
}

void Engine::setCamera( const Vec2f &position ){
	_instance->_graphics->setCamera( position );
}

Vec2f Engine::getCamera(){
	return _instance->_graphics->getCamera();
}

Vec2f Engine::getWorldPosition(const Vec2f &screenPosition){
	return screenPosition-getCamera();
}

void Engine::setLights( const float *lightData, int lightCount ){
	_instance->_graphics->setLights( lightData, lightCount );
}

bool Engine::isPointVisible( const Vec2f &pos ){
	return pos.x > _instance->_screenCorners[0].x && pos.x < _instance->_screenCorners[3].x && pos.y > _instance->_screenCorners[0].y && pos.y < _instance->_screenCorners[2].y;
}

void Engine::refreshShaders(){
	_instance->_graphics->refreshShaders();
}

int Engine::getScreenWidth() { return _instance->_screenWidth; }
int Engine::getScreenHeight() { return _instance->_screenHeight; }
