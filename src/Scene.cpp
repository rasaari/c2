#include "Scene.h"
#include "Engine.h"
#include "Console.h"
#include <algorithm>

/**
 * Inserts an Entity to the scene to be drawn.
 * 
 * Entity with empty tileset will be ignored.
 * Entity already in the scene will be ignored.
 * 
 * @param entity Pointer to the Entity.
 */
void Scene::insert( Entity *entity ) {
	if( entity->tileset.empty() )
		return;
	
	_objects[ entity->tileset ].insert( entPair( entity->getIdInt(), entity ) );
	_objectsById.insert( entPair( entity->getIdInt(), entity ) );
}

/**
 * Removes an Entity from the Scene and frees it from memory
 * 
 * Unknown ObjectId will be ignored.
 * 
 * @param ObjectId Entity id
 */
void Scene::erase( int ObjectId ){
	entMap::iterator entityIt;
	
	if( ( entityIt = _objectsById.find( ObjectId ) ) != _objectsById.end() ){
		_objects[ entityIt->second->tileset ].erase( ObjectId );
		char buffer[50];
		sprintf( buffer, "Removed entity %d", ObjectId );
		std::string logtext = buffer;
		Console::log( logtext );
		delete entityIt->second;
		entityIt->second = 0;
		_objectsById.erase( entityIt );
	}
}

/**
 * Searches the Scene for Entity and returns a pointer to it
 * 
 * Unknown ObjectId will be ignored.
 * 
 * @param ObjectId Entity id
 * @return pointer to Entity or NULL if not found
 */
Entity* Scene::get( int ObjectId ) const{
	entMap::const_iterator eIt;
	if((eIt = _objectsById.find(ObjectId)) != _objectsById.end()){
		return eIt->second;
	}
	return NULL;
}

void Scene::clear(){
	entMap::iterator eIt = _objectsById.begin();
	while(eIt != _objectsById.end()){
		delete (eIt->second);
		eIt->second = 0;
		++eIt;
	}
	
	_objectsById.clear();
	_objects.clear();
	
	for (int i = 0; i < _lights.size(); i++) {
		delete (_lights[i]);
		_lights[i] = 0;
	}
	
	_lights.clear();
	
	std::vector<float> zeroVector;
	zeroVector.resize(104,0);
	Engine::setLights(zeroVector.data(),0);
	
	Engine::setCamera({0.0f,0.0f});
	
	Engine::clearCurrentTilemap();
}

void Scene::addLight( float x, float y, float z ){
	Light* light = new Light( x, y, z );
	_lights.push_back(light);
}

void Scene::addLight( Light *light ){
	_lights.push_back(light);
}

/**
 * TODO: get 8 nearest lights with distance culling.
 * 
 * @return 
 */
float* Scene::getLights(){
	this->_lightsData.clear();
	std::map<float,Light*> lightSorted;
	
	for (int i = 0; i < _lights.size(); i++) {
		if(_lights[i]->isEnabled())
			lightSorted.insert(std::pair<float,Light*>(_lights[i]->getPosition().distance(player->getPosition()), _lights[i]));
	}
	
	int maxLights = std::min((int)_lights.size(),4);
	int loopCount = 0;
	std::map<float,Light*>::iterator lightIt = lightSorted.begin();
	for (lightIt; lightIt != lightSorted.end(); ++lightIt) {
		if(loopCount >= maxLights)
			break;
		
		if(lightIt->first < 1000){
			_lightsData.insert(_lightsData.end(), lightIt->second->getShaderData(), lightIt->second->getShaderData()+13);
			loopCount++;
		}
	}
	
	if(_lightsData.size() < 104){
		_lightsData.resize(104,0);
	}
	
	return this->_lightsData.data();
}

bool Scene::isEmpty() {
	return this->_objectsById.empty();
}

void Scene::render(){
	/* Move camera */
	Engine::setCamera( this->camera );

	/* Pass lights to Engine */
	Engine::setLights( this->getLights(), this->_lights.size());

	/* Render Tilemap */
	this->map.render();

	/* Create a batch out of Scene objects */
	std::map<std::string,entMap>::iterator it;
	for(it = _objects.begin(); it != _objects.end(); ++it) {
		for(entMap::iterator vec = it->second.begin(); vec != it->second.end(); ++vec){
			vec->second->render();
		}
	}
	/* Render the batch */
	Engine::drawEntityBatches();
}

void Scene::moveCamera( const Vec2f &delta ){
	this->camera += delta;
}

void Scene::setCamera( const Vec2f &position ){
	this->camera = position;
}

//used when following player or looking at some other entity
void Scene::centerTo(const Vec2f &pos){
	this->camera.x = -pos.x+Engine::getScreenWidth()/2;
	this->camera.y = -pos.y+Engine::getScreenHeight()/2;
}
