#include "Graphics.h"
#include "GL/glew.h"

Graphics::Graphics() {
	_simpleTileShader = new TileShader( "simple.vert", "simple.frag" );
	_simpleTileShader->create();
	
	_complexTileShader = new TileShader( "mesh.vert","mesh.frag" );
	_complexTileShader->create();
	
	_tileShader = _complexTileShader;

	_textShader = new TextShader( "text.vert","text.frag" );
	_textShader->create();
	
	_entityPainter = new EntityPainter( _tileShader, 9, GL_QUADS );
	_tilemapPainter = new TilemapPainter (_tileShader, 9, GL_TRIANGLES );
	_textPainter = new TextPainter (_textShader, 5, GL_QUADS );
	
	_camera = Vec2f( 0, 0 );
}

Graphics::Graphics(const Graphics& orig) {
}

Graphics::~Graphics() {
	delete _simpleTileShader;
	delete _complexTileShader;
	delete _textShader;
	delete _entityPainter;
	delete _tilemapPainter;
	delete _textPainter;
}

void Graphics::setSimpleShader(bool enabled){
	if(enabled){
		_tileShader = _simpleTileShader;
	}
	else{
		_tileShader = _complexTileShader;
	}
	
	_entityPainter->setShader(_tileShader);
	_tilemapPainter->setShader(_tileShader);
}

void Graphics::drawNPC( const NPC *entity ){
	_entityPainter->npc( entity );
}

void Graphics::drawTile( const Tile *entity ){
	_entityPainter->tile( entity );
}

void Graphics::drawEntityPainter(){
	_entityPainter->render();
}

void Graphics::drawString( float x, float y, const char *str ){
	_textPainter->drawString(x,y,str);
}

void Graphics::setFont( const std::string &alias ){
	_textPainter->setFont( alias );
}
	
void Graphics::drawTextPainter(){
	_textPainter->render();
}

void Graphics::setupTileMap( const std::vector<Tile> &tiles ){
	_tilemapPainter->generate( tiles );
}

void Graphics::drawTileMap(){
	_tilemapPainter->render();
}

void Graphics::clearTileMap(){
	_tilemapPainter->clear();
}

void Graphics::setCamera( const Vec2f &position ){
	_camera = position;
	glTranslatef( _camera.x, _camera.y, -30 );
	_tileShader->setViewMatrix( _camera.x, _camera.y, -30 );
}

Vec2f Graphics::getCamera(){
	return _camera;
}

void Graphics::setLights( const float *lightData, int lightCount ){
	_tileShader->setLights( lightData, lightCount );
}

void Graphics::refreshShaders(){
	_textShader->refresh();
	_tileShader->refresh();
}
