/*
 * This class manages everything related to gamedata files
 * that are in binary format, like map files.
 */

#ifndef _BINARYIO_H
#define _BINARYIO_H

#include <stdio.h>
#include <cstdio>
#include <cstring>
#include <string>
#include <fstream>

union charInt{
	int num;
	unsigned char bytes[sizeof(int)];
};

union charShort{
	short int num;
	unsigned char bytes[sizeof(short int)];
};

class BinaryIO {
private:
	FILE * file;
	char * data;
	int cursor;
	int mapSize;
	
	//deprecated
	void swapByteOrder(unsigned char * target, int size);
public:
	bool EndOfFile;
	bool writeInt(int val);
	bool writeShort(short int val);
	bool writeChar(char val);
	bool open(std::string filename);

	void close();
	bool read(std::string filename);
	
	char nextChar();
	std::string nextString();
	int nextInt();
	short nextShort();
};

#endif