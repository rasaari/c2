#include "XMLTilemapImporter.h"
#include "object/Tile.h"
#include "Console.h"
#include "Light.h"

XMLTilemapImporter::XMLTilemapImporter() {
}

XMLTilemapImporter::~XMLTilemapImporter() {
}

bool XMLTilemapImporter::import( std::string mapName, Scene& scene ) {
	_scene = &scene;
	_map = &(_scene->map);


	// Initialize scene;
	//_scene->clear();

	// Initialize map
	_map->clear();
	_map->tileSize = 48;

	XMLDocument doc;
	
	// form string for map's path
	stringstream ss;
	ss << "./data/maps/" << mapName << ".c2xmap";

	// parse xml
	doc.LoadFile(ss.str().c_str());

	if(doc.Error()) {
		int code = doc.ErrorID();

		stringstream msg;
		msg << "Error while parsing XML! code: " << code << " while loading " << ss.str();
		Console::log(msg.str());

		return false;
	}

	// Find topmost element
	_root = doc.FirstChildElement("map");

	// if not found, map is invalid.
	if(!_root) {
		return false;
	}

	_readTilesets();
	_readTiles();
	_readLights();

	Console::log("XML load finished.");
	return true;
}

void XMLTilemapImporter::_readTilesets() {
	XMLElement* tilesets = _root->FirstChildElement("tilesets");

	// There is no point continuing if map does not have any tilesets.
	if(!tilesets || tilesets->NoChildren()) {
		Console::log("No tilesets found!");

		return;
	}

	XMLNode* tilesetNode = tilesets->FirstChild();

	// Loop until no valid tilesetNode can be found.
	while(tilesetNode) {
		XMLElement* tilesetEl = tilesetNode->ToElement();
		
		if(tilesetEl && string(tilesetEl->Name()) == "tileset") {
			string tilesetName = string(tilesetNode->ToElement()->Attribute("name"));
			_map->insertTileset( tilesetName );
		}
		
		tilesetNode = tilesetNode->NextSibling();
	}
}

void XMLTilemapImporter::_readTiles() {
	
	XMLElement* layersEl = _root->FirstChildElement("layers");

	if(!layersEl || layersEl->NoChildren()) {
		Console::log("No layers found!");

		return;
	}

	XMLNode* layerNode = layersEl->FirstChild();

	while(layerNode) {
		XMLElement* layerEl = layerNode->ToElement();
		if(layerEl && string(layerEl->Name()) == "layer") {
			int z = layerNode->ToElement()->IntAttribute("z");

			if(!layerNode->NoChildren()) {
				XMLNode* tileNode = layerNode->FirstChild();
				
				while(tileNode) {
					Tile tile;
					
					XMLElement* tileEl = tileNode->ToElement();
					
					if(tileEl && string(tileEl->Name()) == "tile") {
						//string tileset = string( tileNode->ToElement()->Attribute( "tileset" ) );
						if(!tileEl->Attribute("tileset")) {
							Console::log("Missing tileset attribute");

							tileNode = tileNode->NextSibling();
							continue;
						}

						string tileset = string(tileEl->Attribute("tileset"));
						tile.tileset = tileset;
						tile.layer = (float)z;

						tile.flags = TILE_FLOOR;

						if(tileEl->Attribute("wall", "true")) {
							tile.flags = TILE_WALL;
						}

						if(!tileEl->Attribute("x") || !tileEl->Attribute("y")) {
							Console::log("Missing x or y attribute");
	
						tileNode = tileNode->NextSibling();
							continue;
						}

						Vec2f tilePos(
							tileEl->FloatAttribute("x"),
							tileEl->FloatAttribute("y")
						);
						
						tilePos *= _map->tileSize;
						
						tile.setPosition(tilePos);
						

						if(!tileEl->Attribute("id")) {
							Console::log("Missing id attribute");

							tileNode = tileNode->NextSibling();
							continue;
						}

						tile.tileId = (short)(tileEl->IntAttribute("id"));

						// Additional attributes
						
						if(tileEl->Attribute("block-width") && tileEl->Attribute("block-height")) {
							int width = tileEl->IntAttribute("block-width");
							int height = tileEl->IntAttribute("block-height");
							
							for(int y = 0; y < height; y++) {
								for(int x = 0; x < width; x++) {
									// Skip the one which is already
									// going to be added.
									if( y == 0 && x == 0) {
										continue;
									}
									
									Tile blockTile;
									blockTile.setPosition({
										tile.getPosition().x + x * _map->tileSize,
										tile.getPosition().y + y * _map->tileSize
									});
									
									blockTile.layer = tile.layer;
									blockTile.tileId = tile.tileId;
									blockTile.tileSize = tile.tileSize;
									blockTile.tileset = tile.tileset;
									blockTile.flags = tile.flags;
									
									_map->insertTile( blockTile );
								}
							}
						}
						
						_map->insertTile(tile);
					}
					tileNode = tileNode->NextSibling();
				}
			}
		}
		
		layerNode = layerNode->NextSibling();
	}
}

void XMLTilemapImporter::_readLights() {
	XMLElement* lightsEl = _root->FirstChildElement("lights");
	
	if(lightsEl && !lightsEl->NoChildren()) {
		XMLNode* lightNode = lightsEl->FirstChild();
		
		while(lightNode) {
			XMLElement* lightEl = lightNode->ToElement();
			
			if(lightEl && string(lightEl->Name()) == "light") {
				float r, g, b;
				
				if(	lightEl->Attribute("x") && 
					lightEl->Attribute("y") &&
					lightEl->Attribute("z") &&
					lightEl->Attribute("r") &&
					lightEl->Attribute("g") &&
					lightEl->Attribute("b") ){
					
					Vec3f lightPos(
						lightEl->FloatAttribute("x"),
						lightEl->FloatAttribute("y"),
						lightEl->FloatAttribute("z")
					);
					
					lightPos *= _map->tileSize;
					
					
					r = lightEl->FloatAttribute("r");
					g = lightEl->FloatAttribute("g");
					b = lightEl->FloatAttribute("b");
					
					Light* light = new Light( lightPos );
					light->setColor( r, g, b);
					
					_scene->addLight(light);
				}
			}
			
			lightNode = lightNode->NextSibling();
		}
	} 
}