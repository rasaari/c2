/* 
 * File:   DeveloperConsole.h
 * Author: jari
 *
 * Created on August 16, 2013, 6:44 PM
 */

#ifndef DEVELOPERCONSOLE_H
#define	DEVELOPERCONSOLE_H

#include <SDL/SDL.h>
#include <string>
#include <map>
#include <vector>
//#include "lua.h"


/*
 * Quake-like developer console where
 * one can use features provided by lua
 */
class Console {
	
public:
	//Single line of input use has given
	static std::string input;
	
	//36 most recent log messages visible in console
	static std::string logHistory[36];
	static std::vector<std::string> commandHistory;
	static int historyIndex;
	
	static void render();
	
	//insert a line to console's log
	static void log(std::string line);
	static void handleEvent(SDL_Event * event);
};

#endif	/* DEVELOPERCONSOLE_H */

