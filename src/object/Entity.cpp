
#include "../GameState.h"
#include "Entity.h"
#include "../GameStateManager.h"

Entity::~Entity(){
}

void Entity::create() {
	if(!GameStateManager::getState()->hasScene)
		return;
	
	GameStateManager::getState()->getScene()->insert(this);
}

void Entity::free() {
	/* Removes from current gamestate->scene */
}

void Entity::render() {
  //do nothing
}

void Entity::setPosition( const Vec2f &pos ){
	this->position = pos;
}

void Entity::setPosition( float x, float y ){
	this->position.x = x;
	this->position.y = y;
}

Vec2f Entity::getPosition() const{
	return this->position;
}

void Entity::move( const Vec2f &pos ){
	this->position += pos;
}

void Entity::move( float x, float y ){
	this->position.x += x;
	this->position.y += y;
}
