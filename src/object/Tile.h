/*
 * tile-class represents a data of a single
 * item in an tile-map. Perhaps logic is not needed
 * here.
 */

#ifndef _TILE_H
#define _TILE_H

#include <string>
#include "Entity.h"

#define TILE_WALL (1<<0)
#define TILE_FLOOR (1<<1)
#define TILE_SHADELESS (1<<2)

class Tile : public virtual Entity {
public:
	int						tileSize;
	int						flags;
	Vec3f					center;
	
							Tile(float x, float y, float z, short tileId, std::string tileset, int flags);
							Tile();
	virtual					~Tile();
	
	bool					isVisible();
	void					render();
	void					setCenter(int tileSize);
};

#endif