/*
 * Entity is a base for all visible ingame objects.
 * It's purpose is to hide most common fields of all
 * objects. It is also so-called root-object, so it can be
 * used as a type in lists and other data-structs.
 */

#ifndef _ENTITY_H
#define _ENTITY_H

#include <string>
#include "../Utility.h"
#include "ObjectId.h"

class Entity {
public:
	float					layer;
	short					tileId;
	std::string				tileset;
	
	void					create();
	void					free();
	void					setPosition( const Vec2f &pos );
	Vec2f					getPosition() const;
	void					move( const Vec2f &pos );
	std::string				getId() { return id.toString(); }
	int						getIdInt() { return id.toInt(); }
	virtual bool			isVisible() = 0;
	virtual					~Entity();
	
	// Deprecated
	virtual void			render();
	void					move( float x, float y );
	void					setPosition( float x, float y );
	
private:
	ObjectId				id;
protected:
	Vec2f					position;
};

#endif