#include "Player.h"

Player::~Player(){
	std::cout << "Player destructor\n";
}

Vec2f				playerLastPos;

void Player::update() {
	_moveSpeed = MOVE_SPEED * Engine::appSpeed();
	
	if( Engine::keyDown( SDLK_UP ) ){
		move( 0, -_moveSpeed );

	/* 
	 * Bounding box top side.
	 * This and the following structures must be replaced by readable function.
	 *
	 * Maybe something like if(_player.overlaps(scene.map.findFloorAt(_player.getPosition())));  
	*/
		if( !_scene->map.findFloorAt( getPosition() + Vec2f( 38,-24 ) ) ||
		!_scene->map.findFloorAt( getPosition() + Vec2f( 10, -24 ) ) ){
			move( 0, _moveSpeed );
		}
	}

	else if( Engine::keyDown( SDLK_DOWN ) ){
		move(0, _moveSpeed);

		/* Bounding box bottom side */
		if(!_scene->map.findFloorAt( getPosition() + Vec2f(10,0)) ||
		!_scene->map.findFloorAt( getPosition() + Vec2f(38,0))){
			move( 0, -_moveSpeed );
		}
	}

	if(Engine::keyDown(SDLK_LEFT)){
		move( -_moveSpeed, 0 );

		/* Bounding box left side */
		if(!_scene->map.findFloorAt( getPosition() + Vec2f(10,0)) ||
		!_scene->map.findFloorAt( getPosition() + Vec2f(10,-24))){
			move(_moveSpeed, 0 );
		}
	}

	else if(Engine::keyDown(SDLK_RIGHT)){
		move( _moveSpeed, 0 );

		/* Bounding box right side */
		if(!_scene->map.findFloorAt( getPosition() + Vec2f(38,0)) ||
		!_scene->map.findFloorAt( getPosition() + Vec2f(38,-24))){
			move(-_moveSpeed,0);
		}
	}

	if(getPosition() != playerLastPos){
		Vec2f dir = getPosition().deltaSquared(playerLastPos);
		_light->setDirection(-dir.x,-dir.y,-0.15);
	}

	playerLastPos = getPosition();

	_light->setPosition( getPosition() + Vec2f( 24,-16 ) );

	if(Engine::keyDown(SDLK_a)){
		_scene->moveCamera({_moveSpeed,0});
	}

	else if(Engine::keyDown(SDLK_d)){
		_scene->moveCamera({-_moveSpeed,0});
	}

	if(Engine::keyDown(SDLK_w)){
		_scene->moveCamera({0,_moveSpeed});
	}

	else if(Engine::keyDown(SDLK_s)){
		_scene->moveCamera({0,-_moveSpeed});
	}

	if(Engine::keyHit(SDLK_F5)){
		Engine::refreshShaders();
	}

	//if(_followPlayer) {
	_scene->centerTo( getPosition() );
	//}
}

void Player::bindScene(Scene* scene) {
	_scene = scene;
}

void Player::unbindScene() {
	_scene = nullptr;
}

void Player::bindLight(Light* light) {
	_light = light;
}

void Player::unbindLight() {
	_light = nullptr;
}