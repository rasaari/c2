#include "Tile.h"
#include "../Engine.h"

Tile::Tile(float x, float y, float z, short tileId, std::string tileset, int flags){
	this->position.x = x;
	this->position.y = y;
	layer = z;

	this->flags = flags;

	tileSize = 48;

	center.x = 0;
	center.y = 0;
	center.z = 0;

	this->tileId = tileId;
	this->tileset = tileset;
}

Tile::Tile(){
	position.x = 0;
	position.y = 0;
	layer = 0;

	tileSize = 0;

	flags = TILE_WALL;
	tileId = 0;
	tileset = "blank";

	center.x = 0;
	center.y = 0;
	center.z = 0;
}

Tile::~Tile(){
}

void Tile::render(){
	Engine::drawTile(this);
}

void Tile::setCenter(int tileSize){
	this->center.x = (this->position.x*2 + tileSize)*0.5;
	this->center.y = (this->position.y*2 + tileSize)*0.5;
	this->center.z = this->layer;
}

bool Tile::isVisible(){
	return true;
}
