#ifndef PLAYER_H
#define	PLAYER_H

#include "NPC.h"
#include "../Scene.h"
#include "../Light.h"

class Scene;

class Player : public NPC {
public:
							Player(int w, int h, const std::string &tileset): NPC(w, h, tileset) {}
	virtual					~Player();
	
	void					update();
	
	void					bindScene( Scene* scene );
	void					unbindScene();
	
	void					bindLight( Light* light);
	void					unbindLight();

private:
	const float				MOVE_SPEED{2.0f};
	Scene*					_scene;
	Light*					_light;
	float					_moveSpeed;
	
};

#endif	/* PLAYER_H */

