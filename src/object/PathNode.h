/* 
 * File:   PathNode.h
 * Author: jari
 *
 * Created on August 27, 2013, 4:07 PM
 */

#ifndef PATHNODE_H
#define	PATHNODE_H

#include "../Utility.h"

class PathNode : public Vec2f {
public:
	float					fScore;
	float					gScore;
	
							PathNode(const Vec2f &position, PathNode *parent, float gScore, float fScore);
							PathNode(const PathNode& orig): _parent(orig._parent), gScore(orig.gScore), fScore(orig.fScore){}
	virtual					~PathNode();
	
	PathNode*				getParent();
	void					setParent(PathNode* parent);

private:
	PathNode*				_parent;
};

#endif	/* PATHNODE_H */

