/* 
 * File:   ObjectId.h
 * Author: jari
 *
 * Created on August 23, 2013, 4:56 PM
 */

#ifndef OBJECTID_H
#define	OBJECTID_H

#include <string>

class ObjectId {
public:
							ObjectId();

	std::string				toString();
	int						toInt();
	
private:
	int						id;
};

#endif	/* OBJECTID_H */

