/* 
 * File:   Path.h
 * Author: jari
 *
 * Created on August 26, 2013, 2:43 PM
 */

#ifndef PATH_H
#define	PATH_H

#include <vector>
#include "../Utility.h"
#include "../Tilemap.h"
#include "PathNode.h"
#include <map>
#include <set>

struct compareFS {
	bool operator()( const PathNode *a, const PathNode *b ) const{
		// TODO: make it fit better
		return (a->fScore < b->fScore) || (a->fScore == b->fScore && a->x < b->x) || (a->fScore == b->fScore && a->x == b->x && a->y < b->y);
	}
};

class Path {
public:
							Path();
	virtual					~Path();
	
	/*
	 * Tells path that current waypoint can be 
	 * removed from the vector.
	 */
	void					waypointReached();
	
	/* 
	 * try to find a path to desired location.
	 * Returns true if path is found, false if not.
	 */
	bool					navigate( Vec2f source, Vec2f destination, const Tilemap &map );
	
	/*
	 * returns true if there are one or
	 * more waypoints in the vector
	 */
	bool					isWaypointsAvailable();
	
	/*
	 * returns pointer to the first
	 * waypoint in the vector if any.
	 * Otherwise null. 
	 */
	Vec2f					getCurrentWaypoint();
	
	void					render();
	void					clearPath();
	
private:
	typedef std::set<PathNode*, compareFS> nodeMapFScore;
	typedef std::map<Vec2f ,PathNode*, Vec2f> nodeMap;
	
	nodeMapFScore			_availableFScore;
	nodeMap					_available;
	nodeMap					closed;
	
	std::vector<Vec2f>		_waypoints;
	float					_tentativeGScore;
	Vec2f					_destination;
	
	void					_openNode(const Vec2f &pos, PathNode *parent, const Tilemap &map);
	void					_clearSets();
};

#endif	/* PATH_H */

