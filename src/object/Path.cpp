/* 
 * File:   Path.cpp
 * Author: jari
 * 
 * Created on August 26, 2013, 2:43 PM
 */

#include "Path.h"
#include <iostream>
#include <cmath>

Path::Path(){
}


Path::~Path(){
	_clearSets();
}

void Path::waypointReached() {
	if( isWaypointsAvailable() ) {
		_waypoints.pop_back();
	}
}

bool Path::navigate( Vec2f source, Vec2f destination, const Tilemap &map ) {
	//TODO: no magic numbers
	source = source.toWaypointGrid(48);
	source.y -= 1;
	
	_destination = destination.toWaypointGrid(48);
	_destination.y -= 1;
	_tentativeGScore = 0;
	
	float gScore = 0;
	float fScore = source.distance(_destination);
	
	//insert initial PathNode (current position of npc)
	PathNode *ptr = new PathNode( source, NULL, gScore, fScore );
	//_available.insert( std::pair<Vec2f, PathNode*>(source, ptr) );
	_available.insert( { source, ptr } );
	_availableFScore.insert(ptr);
	
	//maximum number of cycles to run pathfinder
	int max = 1000;
	
	//current cycle
	int c = 0;
	
	//run while no more available tiles can be found or..
	while(!_available.empty() && c < max) {
		c++;
		
		//get location of one pathnode in available nodes
		nodeMapFScore::iterator afIt = _availableFScore.begin();
		
		//get it and remove it from availables
		PathNode *current = *afIt;
		_available.erase(Vec2f(current->x,current->y));
		_availableFScore.erase(afIt);
		
		//current node can be also added to closed set
		closed.insert(std::pair<Vec2f, PathNode*>(Vec2f(current->x, current->y), current));

		if(_destination == *current) {
			_waypoints.push_back(Vec2f(current->x,current->y));
			PathNode *p;
			
			p = current->getParent();
			
			if(p == NULL){
				break;
			}
			_waypoints.push_back(Vec2f(p->x,p->y));
			while((p = p->getParent()) != NULL) {
				_waypoints.push_back(Vec2f(p->x,p->y));
			}
			_clearSets();
			return true;
		}
		bool floorAt[8];
		Vec2f adjecent[8];
		
		adjecent[0] = Vec2f(current->x+48, current->y);
		adjecent[1] = Vec2f(current->x+48, current->y+48);
		adjecent[2] = Vec2f(current->x, current->y+48);
		adjecent[3] = Vec2f(current->x-48, current->y+48);
		adjecent[4] = Vec2f(current->x-48, current->y);
		adjecent[5] = Vec2f(current->x-48, current->y-48);
		adjecent[6] = Vec2f(current->x, current->y-48);
		adjecent[7] = Vec2f(current->x+48, current->y-48);
		
		int prev,next;
		for (int i = 0; i < 8; i++) {
			floorAt[i] = map.findFloorAt(adjecent[i]);
		}
		
		for (int i = 0; i < 8; i++) {
			if(!floorAt[i])
				continue;
			
			if(i%2 == 1){
				prev = i-1;
				next = (i+1)%8;
				if(!floorAt[prev] || !floorAt[next])
					continue;
			}
			_openNode(adjecent[i], current, map);
		}	
	}
	
	_clearSets();
	return false;
}

bool Path::isWaypointsAvailable() {
	return !( _waypoints.empty() );
}

Vec2f Path::getCurrentWaypoint() {
	if(this->isWaypointsAvailable())
		return _waypoints.back();
	else
		return 0;
}

void Path::_openNode( const Vec2f &pos, PathNode *parent, const Tilemap &map ) {
	nodeMap::iterator cIt = closed.find(pos);
	
	/* Node is unwalkable or in the closed set - skip */
	if(cIt != closed.end())
		return;
	
	float G = parent->gScore + parent->distance(pos);
	
	nodeMap::iterator aIt = _available.find(pos);
	
	/* Add the node to open set */
	if(aIt == _available.end()){
		float F = G + pos.distance(_destination);
		PathNode *ptr = new PathNode(pos, parent, G, F);
		_available.insert(std::pair<Vec2f, PathNode*>(pos, ptr));
		_availableFScore.insert(ptr);
	}
	/* Node is already in the open set */
	else{
		/* Check if path from current parent to the tile is shorter */
		if(G < aIt->second->gScore){
			_availableFScore.erase(aIt->second);

			aIt->second->setParent(parent);
			aIt->second->gScore = G;
			aIt->second->fScore = G + pos.distance(_destination);

			_availableFScore.insert(aIt->second);
		}
	}
}

void Path::render() {
	/*
	glPointSize(3);
	glBegin(GL_POINTS);
	
	glColor3f(0.0f, 1.0f,0.0f);
	nodeMap::iterator ait = available.begin();
	for(ait; ait != available.end(); ++ait){
		glVertex3f(ait->second->x,ait->second->y,7);
	}
	glPointSize(1);
	glColor3f(1.0f, 0.0f,0.0f);
	nodeMap::iterator cit = closed.begin();
	for(cit; cit != closed.end(); ++cit){
		glVertex3f(cit->second->x,cit->second->y,8);
	}		
	glEnd();
	*/
	
	glBegin(GL_LINE_STRIP);
	for(int i = 0; i < _waypoints.size(); i++){
		float color = 1.0/(float)_waypoints.size()*i;
		glColor3f(1.0-color, color,0.0f);
		glVertex3f(_waypoints[i].x+24,_waypoints[i].y-24,2.9);
	}
	glEnd();
}

void Path::clearPath() {
	_waypoints.clear();
}

void Path::_clearSets(){
	nodeMap::iterator ait = _available.begin();
	nodeMap::iterator cit = closed.begin();
	
	for(ait; ait != _available.end(); ++ait){
		delete ait->second;
		ait->second = 0;
	}
	for(cit; cit != closed.end(); ++cit){
		delete cit->second;
		cit->second = 0;
	}

	_available.clear();
	_availableFScore.clear();
	closed.clear();
}