#include "NPC.h"
#include <iostream>
#include "../Engine.h"
#include "../ResourceManager.h"
#include "../Tilemap.h"

NPC::NPC(int w, int h, const std::string &tileset){
		size.x = w;
		size.y = h;
		
		position.x = 0;
		position.y = 0;
		layer = 3;
		
		tileId = 0;
		this->tileset = tileset;
}

NPC::NPC(){
		size.x = 48;
		size.y = 96;
		
		position.x = 0;
		position.y = 0;
		layer = 3;
		
		tileId = 0;
		tileset = "blank";
}

NPC::NPC(const NPC& orig){
}

NPC::~NPC(){

}

void NPC::render() {
	//this->tileId = ResourceManager::getSpriteId(this->tileset, this->animation, this->tileId);
	
	if(Engine::isPointVisible(this->position) || Engine::isPointVisible(Vec2f(this->position.x+this->size.x ,this->position.y-this->size.y))){
		// Synchronize animation updates to engine ticks, so it's FPS independent.
		if(Engine::tick() == true){
			this->animator.animate(this->position);
			this->tileId = ResourceManager::getSpriteId(this->tileset, this->animator.getSpriteEnum(), this->animator.getAnimationId());
		}
		
		Engine::drawNPC(this);
	}
	//path.render();
}

void NPC::clearPath(){
	_path.clearPath();
}

void NPC::gotoWaypoint(const Tilemap &map){
	if(!_path.isWaypointsAvailable()) {
		Vec2f temp(rand()%3000-1500,rand()%3000-1500);

		if(map.findFloorAt(temp))
			_path.navigate(this->position, temp, map);
		
		return;
	}
	
	Vec2f delta = _path.getCurrentWaypoint();
	delta -= this->position;
	
	float length = delta.length();
	if(length < 2.5){
		this->setPosition(_path.getCurrentWaypoint());
		_path.waypointReached();
		return;
	}
	
	this->move(this->position.deltaSquared(_path.getCurrentWaypoint())*Engine::appSpeed()*1.5);
}

bool NPC::isVisible(){
	return false;
}