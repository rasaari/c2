/* 
 * File:   ObjectId.cpp
 * Author: jari
 * 
 * Created on August 23, 2013, 4:56 PM
 */

#include "ObjectId.h"

#include "../Engine.h"
#include <sstream>

ObjectId::ObjectId(){
	id = Engine::getNewId();
}

std::string ObjectId::toString() {
	std::stringstream ss;
	ss << id;
	return std::string( ss.str() );
}

int ObjectId::toInt(){
	return this->id;
}
