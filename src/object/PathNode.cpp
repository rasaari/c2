/* 
 * File:   PathNode.cpp
 * Author: jari
 * 
 * Created on August 27, 2013, 4:07 PM
 */

#include "PathNode.h"
#include <iostream>
PathNode::PathNode(const Vec2f &position, PathNode *parent, float gScore, float fScore){
	this->x = position.x;
	this->y = position.y;
	this->_parent = parent;
	this->gScore = gScore;
	this->fScore = fScore;
}

PathNode::~PathNode(){

}

PathNode* PathNode::getParent() {
	return _parent;
}

void PathNode::setParent(PathNode* parent){
	_parent = parent;
}
