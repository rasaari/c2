/**
 * Non-player controlled character. Basically all AI-objects like
 * villagers, shopkeepers, animals etc. in game world.
 */
#ifndef NPC_H
#define	NPC_H

#include "Entity.h"
#include "../Animator.h"
#include "Path.h"

// Forward declaration
class Tilemap;

class NPC : public Entity {
public:
	Vec2i					size;
	Animator				animator;

							NPC( int w, int h, const std::string &tileset );
							NPC();
							NPC( const NPC& orig );
							
	virtual					~NPC();
	
	virtual void			render();
	bool					isVisible();
	void					clearPath();
	void					gotoWaypoint( const Tilemap &map );
	
private:
	std::string				_name;
	Path					_path;
};

#endif

