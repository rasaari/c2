#ifndef GRAPHICS_H
#define	GRAPHICS_H

#include "shader/TileShader.h"
#include "shader/TextShader.h"

#include "renderable/EntityPainter.h"
#include "renderable/TilemapPainter.h"
#include "renderable/TextPainter.h"

#include "Utility.h"

class Graphics {
public:
						Graphics();
						Graphics( const Graphics& orig );
	virtual					~Graphics();
	
	void					setSimpleShader( bool enabled );
	
	void					drawNPC( const NPC *entity );
	void					drawTile( const Tile *entity );
	void					drawEntityPainter();
	
	void					drawString( float x, float y, const char *str );
	void					setFont( const std::string &alias );
	void					drawTextPainter();
	
	void					setupTileMap( const std::vector<Tile> &tiles );
	void					drawTileMap();
	void					clearTileMap();
	
	void					setCamera( const Vec2f &position );
	Vec2f					getCamera();
	void					setLights( const float *lightData, int lightCount );
	
	void					refreshShaders();

private:
	EntityPainter*				_entityPainter;
	TilemapPainter*				_tilemapPainter;
	TextPainter*				_textPainter;
	
	TileShader*				_tileShader;
	TileShader*				_simpleTileShader;
	TileShader*				_complexTileShader;
	TextShader*				_textShader;
	
	Vec2f					_camera;
};

#endif	/* GRAPHICS_H */

