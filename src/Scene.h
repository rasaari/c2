/*
 * Scene is stage, where game happens.
 *
 * Scene takes care of rendering order and rendering itself.
 * Objects that belongs to scene must inherit Entity-baseobject.
 * That also mean that Scene can have tiles, NPCs, particles and so on.
 */

#ifndef _SCENE_H
#define _SCENE_H

#include <sstream>
#include <algorithm>
#include <vector>
#include <string>
#include "object/Entity.h"
#include "object/Player.h"
#include "Engine.h"
#include "Tilemap.h"
#include "Light.h"

/*
#include <unistd.h>
#include <chrono>
*/
typedef struct WorldData {
  int world, area;
  std::string name;
} WorldData;

class Tilemap;
class Player;

class Scene{
public:
	//methods
	void					insert( Entity *entity );
	void					erase( int ObjectId );
	Entity*					get( int ObjectId ) const;
	void					clear();
	void					render();
	void					addLight( float x, float y, float z );
	void					addLight( Light* light );
	float*					getLights();
	
	bool					isEmpty();

	void					moveCamera( const Vec2f &delta );
	void					setCamera( const Vec2f &position );
	void					centerTo( const Vec2f &pos );

	//public fields
	Tilemap					map;
	float					camX{0.0f}, camY{0.0f};
	Vec2f					camera{0.0f, 0.0f};
	Player*					player;
	//constructors & destructor

	virtual					~Scene(){};

private:
	typedef std::pair<int,Entity*> entPair;
	typedef std::map<int,Entity*> entMap;
	typedef std::map<std::string, entMap> ObjectMap;
	
	WorldData				_worldData;
	ObjectMap				_objects;
	entMap					_objectsById;
	std::vector<Light*>		_lights;
	std::vector<float>		_lightsData;
};

#endif // _SCENE_H
