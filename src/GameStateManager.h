#ifndef _GAMESTATEMANAGER_H
#define _GAMESTATEMANAGER_H

#include <SDL/SDL.h>
#include <vector>
#include "GameState.h"
#include "gameStates/Game.h"

//states
enum {
	GS_GAME = 0
};

class GameStateManager {		
public:
	GameState_Game			gsGame;
	GameState*				activeGameState;
	
	//static GameStateManager();
	static void				setState( int );
	static void				render();
	static void				handleEvent( SDL_Event* event );
	static void				update();
	static void				callInit();

	static void				init();
	
	static GameState*		getState();
	
	//static GameStateManager *instance;
	static GameStateManager& getInstance() {
		static GameStateManager instance;
		
		return instance;
	}
private:
	/* GameStateManager() {} */
};

#endif