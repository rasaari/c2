/*
 * Resource manager is the only place where
 * loading of images or audio files is allowed.
 *
 * basic idea is that images are loaded to
 * array-like structure called map. Then every
 * image file is mapped with an alias. It makes possible
 * that complex filenames can be replaces with simple
 * string.
 *
 * example:
 * glImages["player"] = "C:\program files\vendor\game\gfx\player.png"
 */

#ifndef _RESOURCEMANAGER_H
#define _RESOURCEMANAGER_H

#include <string>
#include <stdio.h>
#include <iostream>
#include <math.h>
#include <map>
#include <vector>
#include "Utility.h"
/**
 * Struct for calculating and storing a tiled texture uv-coordinates.
 */
struct UV4 {
	/**
	 * u0, v0 = upper left corner
	 * 
	 * u1, v1 = lower right corner 
	 */
	float u0, u1, v0, v1;

	/**
	 * Generates uv-coordinates
	 * 
	 * @param w texture width
	 * @param h texture height
	 * @param id
	 * @param tileSizeX tile width
	 * @param tileSizeX tile height
	 */
	UV4(float w, float h, int id, int tileSizeX, int tileSizeY);
	UV4();
};

typedef unsigned int GLuint;

class ResourceManager {
public:
	enum animation {
		WALK_0, // 1
		WALK_45,
		WALK_90, // 3
		WALK_135,
		WALK_180, // 5
		WALK_225,
		WALK_270, // 7
		WALK_315,

		IDLE_0, // 1
		IDLE_45,
		IDLE_90, // 3
		IDLE_135,
		IDLE_180, // 5
		IDLE_225,
		IDLE_270, // 7
		IDLE_315,
	};
	
	/**
	 * Struct for storing font texture data
	 * 
	 * Populated automatically with loadFontTexture
	 */
	struct fontData {
	public:
		int kerning;
		Vec2i letterDimensions;
		fontData();
	};
	
	static void init();
	static bool loadTilesetTexture(std::string alias, std::string filename, int tileSize = 48);
	static bool loadSpriteTexture(std::string alias, std::string filename);
	static bool loadImageTexture(std::string alias, std::string filename);
	static bool loadFontTexture(std::string alias, std::string filename, int kerning = 0);
	static void freeImages();
	static void freeImage(const std::string &alias);
	static GLuint getImage(const std::string &alias);
	static void loadBlankImage();
	static Vec2i getImageDimensions(const std::string &alias);
	static const UV4* getImageUV(const std::string &alias, int id);
	static int getSpriteId(const std::string &alias, ResourceManager::animation aniEnum, int idOffset);
	static const fontData* getFontData(const std::string &alias);
	int getFontKerning(const std::string &alias);
	int getFontWidth(const std::string &alias);
	int getFontHeight(const std::string &alias);

private:
	static const std::string _tilesetPath;
	static const std::string _spritePath;
	static const std::string _imagePath;
	static const std::string _fontPath;

	/**
	 * Struct for storing texture data
	 * 
	 * This will be populated by loading a .c2anim file in the future.
	 */
	struct imageData {
	private:
		GLuint _glTextureId;
		Vec2i _size;
		Vec2i _tileSize;
		std::vector<UV4> _uvs;
		int _uvMax;

		void _generateMapUV();

	public:
		imageData();
		imageData(GLuint glTextureId, const Vec2i &size, const Vec2i &tileSize);
		int width();
		int height();
		Vec2i dimensions();
		GLuint id();
		void free();
		const UV4 * uv(int i);
	};

	/**
	 * Struct for storing a spriteset animation data
	 * 
	 * Populated by loading a .c2anim file with importSpriteData()
	 */
	struct spriteData {
	private:
		struct oneAnimation {
			std::vector<int> ids;
			int count;
		};
		std::map<ResourceManager::animation, oneAnimation > _animations;
	public:
		int spriteWidth, spriteHeight;
		spriteData();
		int id(ResourceManager::animation spriteEnum, int offset);
		void add(ResourceManager::animation aniEnum, std::vector<int> &ids);
	};

	/* All images and their basic data */
	static std::map<std::string, imageData> _glImages;
	/* Iterator for glImages */
	typedef std::map<std::string, imageData>::iterator glImageIt;
	/* Animation data for sprites */
	static std::map<std::string, spriteData> _glSpritesData;
	/* Font data for Font textures */
	static std::map<std::string, fontData> _glFontsData;
	/* Animation enum lookup std::map by .c2anim string */
	static std::map<std::string, ResourceManager::animation> _animEnumStrings;

	static bool _loadImage(std::string alias, std::string source, int tileSizeX = 48, int tileSizeY = 48);
	static bool _importSpriteData(std::string alias, std::string imgFilePath);
	static void _setFontData(const std::string &alias, int kerning);
};

#endif