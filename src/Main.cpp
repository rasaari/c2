#ifndef __C2_LIB__

#include "Engine.h"
//#include "lua.h"

/**
 * Starting point of the program
 *
 * @param argc
 * @param argv
 * @return
 */
int main( int argc, char* argv[] ) {
	boost::filesystem::current_path(boost::filesystem::system_complete(argv[0]).parent_path());

	Engine engine;
	if( argc > 1 ) {
		for( int i = 1; i < argc; i++ ) {
			std::string arg = argv[i];

			if( ( arg == "--map" || arg == "-m" ) && argc > i+1 ) {
				std::string map = argv[++i];
				engine.testMap(map);
			}

			else if( arg == "--simple-shader" || arg == "-S" ){
				engine.setSimpleShader( true );
			}
		}
	}

	//lua::init();
	//luaL_dofile(luaState, "data/scripts/init.lua");
	
	if(engine.start() == 0){
		while(engine.run()){
			
		}
		engine.cleanUp();
	}
	
	//Run game. The engine's exit code is also the exit code of the whole program.
	return 0;
}

#endif
