#include "TileShader.h"

TileShader::~TileShader(){
}

void TileShader::setAttributes(){
	glBindAttribLocation(this->program,0, "OBJ_Position");
	glBindAttribLocation(this->program,1, "OBJ_Texcoord");
	glBindAttribLocation(this->program,2, "OBJ_Center");
	glBindAttribLocation(this->program,3, "OBJ_TexUnit");
}

void TileShader::setUniforms(){
	int location;
	location = glGetUniformLocationARB(this->program, "Difftexture0");
	glUniform1i(location, 0);
	location = glGetUniformLocationARB(this->program, "Difftexture1");
	glUniform1i(location, 1);
	location = glGetUniformLocationARB(this->program, "Difftexture2");
	glUniform1i(location, 2);
	location = glGetUniformLocationARB(this->program, "Difftexture3");
	glUniform1i(location, 3);
	
	float rVecX = 1,rVecY = 0,rVecZ = 0;
	float rotation = 3.14162 * 0.50; // 90deg
	float sinAng = sinf(rotation);
	float cosAng = cosf(rotation);
	float uAng = 1.0 - cosAng;
	
	float initWallRotationMatrix[] = {
		rVecX*rVecX*uAng + cosAng, rVecX*rVecY*uAng + (rVecZ*sinAng), rVecX*rVecZ*uAng - (rVecY*sinAng),
		rVecY*rVecX*uAng - (rVecZ*sinAng), rVecY*rVecY*uAng + cosAng, rVecX*rVecZ*uAng - (rVecY*sinAng),
		rVecZ*rVecX*uAng + (rVecY*sinAng), rVecZ*rVecY*uAng - (rVecX*sinAng), rVecZ*rVecZ*uAng + cosAng
	};
	
	
	glUniformMatrix3fv(this->wallMatrixLocation, 1, GL_FALSE, initWallRotationMatrix);
	
	float initViewMatrix[] = {
		1,0,0,0,
		0,1,0,0,
		0,0,1,0,
		0,0,0,1,
	};
	this->viewMatrix.assign(initViewMatrix,initViewMatrix+16);
	glUniformMatrix4fv(this->viewMatrixLocation, 1, GL_FALSE, this->viewMatrix.data());
	
	//glUniform1iARB(this->lightCountLocation, (GLint)0);
	
	this->lights.assign(104,1);
	glUniform1fv(this->lightsLocation, 104, this->lights.data());
}

void TileShader::preSetUniforms(){
	this->wallMatrixLocation = glGetUniformLocation(this->program, "WallMatrix");
	this->viewMatrixLocation = glGetUniformLocation(this->program, "ViewMatrix");
	//this->lightCountLocation = glGetUniformLocation(this->program, "LightCount");
	this->lightsLocation = glGetUniformLocation(this->program, "LightsArray");
}

void TileShader::enableChild(){
	glUniform1fv(this->lightsLocation, 104, this->lights.data());
	//glUniform1i(this->lightCountLocation, this->lightCount);
	glUniformMatrix4fv(this->viewMatrixLocation, 1, GL_FALSE, this->viewMatrix.data());
	
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
}

void TileShader::disableChild(){
	glDisableVertexAttribArrayARB(0);
	glDisableVertexAttribArrayARB(1);
	glDisableVertexAttribArrayARB(2);
	glDisableVertexAttribArrayARB(3);
}

void TileShader::setVertexPointers(){
	glVertexAttribPointerARB(0, 3, GL_FLOAT, false, 36, (const GLvoid*)0);
	glVertexAttribPointerARB(1, 2, GL_FLOAT, false, 36, (const GLvoid*)12);
	glVertexAttribPointerARB(2, 3, GL_FLOAT, false, 36, (const GLvoid*)20);
	glVertexAttribPointerARB(3, 1, GL_FLOAT, false, 36, (const GLvoid*)32);
}

/**
 * lightArray size must be exactly 104.
 * 
 * @param lightArray array to be copied.
 */
void TileShader::setLights(const float* lightArray, int count){
	this->lights.assign(lightArray,lightArray+104);
	this->lightCount = count;
}

void TileShader::setViewMatrix(float x, float y, float z){
	this->viewMatrix[3] = x;
	this->viewMatrix[7] = y;
	this->viewMatrix[11] = z;
}
