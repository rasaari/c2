#include "Shader.h"
#include "../BinaryIO.h"
#include "../ResourceManager.h"

Shader::~Shader(){
	this->deleteShaderObjects();
	this->deleteProgram();
}

bool Shader::createProgram(){
	this->vertShader = this->createShader(GL_VERTEX_SHADER_ARB, this->vertPath);
	this->fragShader = this->createShader(GL_FRAGMENT_SHADER_ARB, this->fragPath);

	if(this->vertShader == 0 || this->fragShader == 0) {
		return false;
	}
	
	this->program = glCreateProgram();
	
	if(this->program == 0) {
		return false;
	}
	
	glAttachObjectARB(this->program, this->vertShader);
	glAttachObjectARB(this->program, this->fragShader);
	
	/* Set Attributes */
	this->setAttributes();
	
	glLinkProgramARB(this->program);
	
	GLint linkStatus;
	glGetProgramiv(this->program, GL_LINK_STATUS, &linkStatus);
	if(linkStatus == GL_FALSE){
		std::cout << "Shader program link status false.\n";
		this->printProgramLog(this->program);
		return false;
	}

	glValidateProgramARB(this->program);
	this->printProgramLog(this->program);
	
	this->preSetUniforms();
	glUseProgram(this->program);

	/* Set Uniforms */
	this->setUniforms();
	
	glUseProgram(0);
	
	std::cout << "Loaded " << this->vertPath << " & " << this->fragPath << "\n";
	return true;
}

void Shader::setAttributes(){
	//Overload this
}

void Shader::setUniforms(){
	//Overload this
	
	int location;
	location = glGetUniformLocationARB(this->program, "Difftexture0");
	glUniform1i(location, 0);
	location = glGetUniformLocationARB(this->program, "Difftexture1");
	glUniform1i(location, 1);
	location = glGetUniformLocationARB(this->program, "Difftexture2");
	glUniform1i(location, 2);
	location = glGetUniformLocationARB(this->program, "Difftexture3");
	glUniform1i(location, 3);
}

void Shader::preSetUniforms(){
}

void Shader::deleteProgram(){
	if(glIsProgram(this->program))
		glDeleteProgram(this->program);
}

void Shader::deleteShaderObjects(){
	if(glIsShader(this->vertShader)){
		glDetachShader(this->program, this->vertShader);
		glDeleteShader(this->vertShader);
	}
	
	if(glIsShader(this->fragShader)){
		glDetachShader(this->program, this->fragShader);
		glDeleteShader(this->fragShader);
	}
}
/**
 * Creates the Shader from the sources given in constructor.
 * Must be called before anything else is done with the Shader.
 * 
 * @return True is Shader is compiled and ready to be used.
 */
bool Shader::create(){
	this->ready = this->createProgram();
	this->deleteShaderObjects();
	if(this->ready == false){
		this->deleteProgram();
	}
	return ready;
}

/**
 * Reloads the Shader program from disk
 * 
 * TODO: Check that the Shader has been created before.
 */
void Shader::refresh(){
	this->deleteProgram();

	this->create();
}

/**
 * Compiles the shader source.
 * 
 * @param type GLenum GL_VERTEX_SHADER_ARB or GL_FRAGMENT_SHADER_ARB
 * @param path Source file name
 * @return shader object handle or 0 if failed.
 */
int Shader::createShader(GLenum type, std::string path){
	GLuint shader = 0;
	
	shader = glCreateShader(type);
	
	if(shader == 0) {
		return 0;
	}
	
	std::string shaderCode;
	this->getSource(path, &shaderCode);
	const char * p = shaderCode.c_str();
	
	glShaderSourceARB(shader, 1, &p, NULL);
	glCompileShaderARB(shader);
	
	GLint shaderStatus;
	glGetShaderiv(shader,GL_COMPILE_STATUS, &shaderStatus);
	
	std::cout << path << " " << shaderStatus << "\n";
	
	return shader;
}

/**
 * Reads vertex/fragment shader source from disk and stores it in the specified std::string.
 * 
 * Files are assumed to be in "./data/shaders/"
 * 
 * @param name Shader source file name.
 * @param str Pointer to std::string for storing the source.
 */
void Shader::getSource(std::string name, std::string * str){
	//path to map file
	std::string path = "./data/shaders/"+name;

	FILE * pFile = fopen(path.c_str(),"r");
	char buffer[300];
	if (pFile != NULL){
		while(fgets(buffer, 299, pFile) != NULL){
			str->append(buffer);	
		}
		fclose (pFile);
	}
}

/**
 * Enables the Shader program for rendering.
 */
void Shader::enable(){
	glUseProgram(this->program);
	this->enableChild();
}

void Shader::enableChild(){
	// Override this
	
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
}

void Shader::disable(){
	// Override this
	
	this->disableChild();
	glUseProgram(0);
}

void Shader::disableChild(){
	glDisableVertexAttribArrayARB(0);
	glDisableVertexAttribArrayARB(1);
}

bool Shader::isReady(){
	return this->ready;
}

void Shader::setVertexPointers(){
	// Override this
	
	glVertexAttribPointerARB(0, 3, GL_FLOAT, false, 20, (const GLvoid*)0);
	glVertexAttribPointerARB(1, 2, GL_FLOAT, false, 20, (const GLvoid*)12);
}

/**
 * Checks the shader log for errors and outputs them if found.
 * 
 * @param obj vertex/fragment shader or whole shader program.
 */
void Shader::printProgramLog(GLuint obj){
	int infologLength = 0;
	int maxLength = 0;

	glGetProgramiv(obj,GL_INFO_LOG_LENGTH,&maxLength);

	char infoLog[maxLength];
	
	glGetProgramInfoLog(obj, maxLength, &infologLength, infoLog);
	
	if (infologLength > 0){
		FILE * pFile = fopen("glsl_log.txt","w");
		if(pFile != NULL){
			fputs(infoLog,pFile);
			fclose(pFile);
		}
		printf("\n%s\n",infoLog);
	}
}

int Shader::getMaxTextures(){
	return this->maxTextures;
}
