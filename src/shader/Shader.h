#ifndef SHADER_H
#define	SHADER_H

#include <GL/glew.h>
#include <string>
#include <iostream>
#include <vector>

class Shader{
private:
	GLhandleARB vertShader, fragShader;
	std::string vertPath, fragPath;
	
	void getSource(std::string name, std::string * str);
	void printProgramLog(GLuint obj);
	int createShader(GLenum type, std::string path);
	bool createProgram();
	void deleteProgram();
	void deleteShaderObjects();
	
protected:
	GLhandleARB program;
	int maxTextures;
	bool ready;
	virtual void setAttributes();
	virtual void setUniforms();
	virtual void preSetUniforms();
	virtual void enableChild();
	virtual void disableChild();
 
public:
	Shader(const char * vertPath, const char * fragPath){
		ready = false;
		maxTextures = 0;
		this->vertPath = vertPath;
		this->fragPath = fragPath;
	}
	bool create();
	void refresh();
	void enable();
	void disable();
	virtual void setVertexPointers();
	bool isReady();
	int getMaxTextures();
	virtual ~Shader();
};
#endif	/* SHADER_H */

