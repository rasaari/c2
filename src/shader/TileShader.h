#ifndef TILESHADER_H
#define	TILESHADER_H

#include "Shader.h"
#include <math.h>

class TileShader : public Shader {

protected:
	
private:
	void setAttributes();
	void setUniforms();
	void preSetUniforms();
	void enableChild();
	void disableChild();
	int lightCountLocation, lightsLocation, viewMatrixLocation, wallMatrixLocation;
	int lightCount;
	std::vector<GLfloat> lights;
	std::vector<GLfloat> viewMatrix;
public:
	TileShader(const char * vertPath, const char * fragPath):Shader(vertPath,fragPath){
		lightCount = 0;
		maxTextures = 4;
	}
	virtual ~TileShader();
	void setVertexPointers();
	void setLights(const float* lightArray, int count);
	void setViewMatrix(float x, float y, float z);
	void setWallRotationMatrix();
};

#endif	/* TILESHADER_H */

