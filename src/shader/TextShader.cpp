#include "TextShader.h"

TextShader::~TextShader() {
}

void TextShader::setAttributes(){
	glBindAttribLocation(this->program,0, "Text_Pos");
	glBindAttribLocation(this->program,1, "Text_UV");
	glBindAttribLocation(this->program,2, "Text_TexUnit");
}

void TextShader::setUniforms(){
	int location;
	location = glGetUniformLocationARB(this->program, "Difftexture0");
	glUniform1i(location, 0);
	location = glGetUniformLocationARB(this->program, "Difftexture1");
	glUniform1i(location, 1);
	location = glGetUniformLocationARB(this->program, "Difftexture2");
	glUniform1i(location, 2);
	location = glGetUniformLocationARB(this->program, "Difftexture3");
	glUniform1i(location, 3);
}

void TextShader::setVertexPointers(){
	glVertexAttribPointerARB(0, 2, GL_FLOAT, false, 20, (const GLvoid*)0);
	glVertexAttribPointerARB(1, 2, GL_FLOAT, false, 20, (const GLvoid*)8);
	glVertexAttribPointerARB(2, 1, GL_FLOAT, false, 20, (const GLvoid*)16);
}

void TextShader::enableChild(){
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
}

void TextShader::disableChild(){
	glDisableVertexAttribArrayARB(0);
	glDisableVertexAttribArrayARB(1);
	glDisableVertexAttribArrayARB(2);
}
