#ifndef TEXTSHADER_H
#define	TEXTSHADER_H

#include "Shader.h"

class TextShader : public Shader{
public:
	TextShader(const char * vertPath, const char * fragPath):Shader(vertPath,fragPath){
		maxTextures = 4;
	}
	virtual ~TextShader();
	void setVertexPointers();
private:
	void setAttributes();
	void setUniforms();
	void enableChild();
	void disableChild();
};

#endif	/* TEXTSHADER_H */

