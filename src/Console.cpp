#include "Console.h"
#include "Engine.h"
#include <iostream>

#define LOG_HISTORY_SIZE 36
#define LEFT_BORDER 10
#define PROMPT ">"

std::string Console::input;
std::string Console::logHistory[LOG_HISTORY_SIZE];
std::vector<std::string> Console::commandHistory;
int Console::historyIndex;

int cursorIndex = 0;

void Console::render() {
	Engine::setFont("Bisasam");
	Engine::drawString(LEFT_BORDER,30, "Developer console");
	Engine::setFont("Terminus");
	
	//draw prompt
	std::string prompt = PROMPT + input;
	Engine::drawString(LEFT_BORDER,600-15, prompt.c_str());
	
	//draw cursor
	Engine::drawString(LEFT_BORDER + cursorIndex*8+3, 600-15, "|");
	
	//draw log
	for(int i = 0; i < LOG_HISTORY_SIZE; i++) {
		Engine::drawString(LEFT_BORDER, 45+15*i, logHistory[i].c_str());
	}
	
}

void Console::log(std::string line) {
	for(int i = 0; i < LOG_HISTORY_SIZE-1; i++) {
		logHistory[i] = logHistory[i+1];
	}
	
	logHistory[LOG_HISTORY_SIZE-1] = line;
}

void Console::handleEvent(SDL_Event * event) {
	if(event->type == SDL_KEYDOWN) {
		if((event->key.keysym.sym == SDLK_BACKSPACE) && (input.length() != 0 )){
			//Remove a character from the end
			//input.erase( input.length() - 1 );
			input.erase(cursorIndex - 1, 1);
			cursorIndex--;
		}
		
		if((event->key.keysym.sym == SDLK_RETURN)) {
			/*
			if(luaL_dostring(luaState, input.c_str())) {
				log("Error: " + std::string(lua_tostring(luaState,-1)));
				lua_pop(luaState, 1);
			}
			*/
			log("not implemented.");
			commandHistory.push_back(input);
			input = "";
			
			cursorIndex = 0;
			historyIndex = commandHistory.size();
		}
		
		if((event->key.keysym.sym == SDLK_UP)) {
			if(historyIndex > 0)
				historyIndex--;

			if(!commandHistory.empty()) {
				input = commandHistory.at(historyIndex);
				cursorIndex = input.length()-1;
			}
		}
		
		if((event->key.keysym.sym == SDLK_LEFT)) {
			if(cursorIndex > 0)
				cursorIndex--;
		}
		
		if((event->key.keysym.sym == SDLK_RIGHT)) {
			if(cursorIndex < input.length())
				cursorIndex++;
		}
		
		if((event->key.keysym.sym == SDLK_DOWN)) {
			if(historyIndex < commandHistory.size())
				historyIndex++;
			
			if(historyIndex == commandHistory.size()){
				input = "";
				cursorIndex = 0;
				return;
			}
			input = commandHistory.at(historyIndex);
			cursorIndex = input.length()-1;
		}
		
		if((event->key.keysym.sym == SDLK_DELETE)) {
			input.erase(cursorIndex, 1);
			//historyIndex = commandHistory.size();
		}
		
		//any visible character key in ASCII
		if( ( event->key.keysym.unicode >= (Uint16)32 ) && ( event->key.keysym.unicode <= (Uint16)126 ) ) {
			//input += (char)event->key.keysym.unicode;
			input.insert(cursorIndex,1 , event->key.keysym.unicode);
			cursorIndex++;
		}
	}
}