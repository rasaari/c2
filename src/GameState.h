#ifndef _GAMESTATE_H
#define _GAMESTATE_H

#include <SDL/SDL.h>
#include "Scene.h"
class GameState {
public:
	virtual void			render() = 0;
	virtual void			update() = 0;
	virtual void			suspend() = 0;
	virtual void			resume() = 0;
	virtual void			handleEvent(SDL_Event* event) = 0;
	virtual void			init() = 0;
	
	virtual Scene*			getScene() = 0;
	
	bool					hasScene;
};

//Not needed?
// void GameState::render() {}
// void GameState::update() {}
// void GameState::suspend(){}
// void GameState::resume() {}

#endif