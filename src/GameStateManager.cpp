#include "GameStateManager.h"

void GameStateManager::init() {
	getInstance().activeGameState = 0;
	

	setState(GS_GAME);
}

void GameStateManager::setState( int gameStateNumber ) {
	if( getState() )  {
		getState()->suspend();
	}
	
	switch( gameStateNumber ) {
	case GS_GAME:
		getInstance().activeGameState = &getInstance().gsGame;
		break;
	}
}

void GameStateManager::render() {
	if( getState() )  {
		getState()->render();
	}
	
}

void GameStateManager::handleEvent( SDL_Event* event ) {
	if( getState() ) {
		getState()->handleEvent(event);
	}
}

void GameStateManager::update() {
	if( getState() ) {
		getState()->update();
	}
}

void GameStateManager::callInit() {
    if( getState() ) {
		getState()->init();
	}
}

GameState* GameStateManager::getState() {
	return getInstance().activeGameState;
}