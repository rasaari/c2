#ifndef UTILITY_H
#define	UTILITY_H

#include <complex>

template<typename X>
class Vec2x{
	public:
	union{
		struct{
			X x,y;
		};
		X a[2];
	};
	inline Vec2x():x(0),y(0){}
	inline Vec2x(X a):x(a),y(a){}
	inline Vec2x(X x, X y):x(x),y(y){}
	inline Vec2x(const Vec2x<X> & other): x(other.x), y(other.y){}
	inline virtual ~Vec2x(){}
	void operator+=(const Vec2x<X> &other){
		this->x += other.x;
		this->y += other.y;
	}
	void operator-=(const Vec2x<X> &other){
		this->x -= other.x;
		this->y -= other.y;
	}
	void operator*=(const Vec2x<X> &other){
		this->x *= other.x;
		this->y *= other.y;
	}
	void operator/=(const Vec2x<X> &other){
		this->x /= other.x;
		this->y /= other.y;
	}
	
	Vec2x<X> operator+(const Vec2x<X> &other) const{
		Vec2x<X> temp(this->x + other.x, this->y + other.y);
		return temp;
	}
	Vec2x<X> operator-(const Vec2x<X> &other) const{
		Vec2x<X> temp(this->x - other.x, this->y - other.y);
		return temp;
	}
	Vec2x<X> operator*(const Vec2x<X> &other) const{
		Vec2x<X> temp(this->x * other.x, this->y * other.y);
		return temp;
	}
	Vec2x<X> operator/(const Vec2x<X> &other) const{
		Vec2x<X> temp(this->x / other.x, this->y / other.y);
		return temp;
	}
	
	bool operator<(const Vec2x<X> &other) const{
		return (this->x < other.x )|| (this->x == other.x && this->y < other.y);
	}
	bool operator<=(const Vec2x<X> &other) const{
		return (this->x <= other.x )|| (this->x == other.x && this->y <= other.y);
	}
	bool operator>(const Vec2x<X> &other) const{
		return (this->x > other.x )|| (this->x == other.x && this->y > other.y);
	}
	bool operator>=(const Vec2x<X> &other) const{
		return (this->x >= other.x )|| (this->x == other.x && this->y >= other.y);
	}
	bool operator==(const Vec2x<X> &other) const{
		return (x == other.x ) && (y == other.y);
	}
	bool operator!=(const Vec2x<X> &other) const{
		return (this->x != other.x ) || (this->y != other.y);
	}
	bool operator()(const Vec2x<X> &first, const Vec2x<X> &second) const{
		return (first.x < second.x ) || (first.x == second.x && first.y < second.y);
	}
	
	
	X length() const{
		X length;
		length = std::sqrt(this->x*this->x+this->y*this->y);
		return length;
	}
	
	X distance(const Vec2x<X> &other) const{
		X distance;
		distance = std::sqrt(std::pow(other.x - this->x,2) + std::pow(other.y - this->y,2));
		return distance;
	}
	
	/**
	 * Normalizes this vector and returns it as a copy
	 * 
         * @return the normalized vector
         */
	Vec2x<X> normalize() const{
		Vec2x<X> temp(*this);
		return temp/temp.length();
	}
	
	/**
	 * Returns the normalized vector of other - this
	 * 
         * @param other 
         * @return 
         */
	Vec2x<X> deltaSquared(const Vec2x<X> &other) const{
		return Vec2x<X>(other.x - this->x, other.y - this->y).normalize();
	}
	
	Vec2x<X> toGrid(int gridSize) const{
		Vec2x<X> temp(*this);
		Vec2x<X> fmodValues(std::fmod(temp.x, gridSize),std::fmod(temp.y, gridSize));
		if(fmodValues.x < 0){
			fmodValues.x += 48;
		}
		if(fmodValues.y < 0){
			fmodValues.y += 48;
		}
		return temp - fmodValues;
	}
	
	Vec2x<X> toWaypointGrid(int gridSize) const{
		Vec2x<X> temp(*this);
		Vec2x<X> fmodValues(std::fmod(temp.x, gridSize),std::fmod(temp.y, gridSize));
		if(fmodValues.x < 0){
			fmodValues.x += 48;
		}
		if(fmodValues.y >= 0){
			fmodValues.y -= 48;
		}
		return temp - fmodValues;
	}
	
	void operator*=(X mul) {
		this->x *= mul;
		this->y *= mul;
	}
};
typedef Vec2x<float> Vec2f;
typedef Vec2x<int> Vec2i;

template<typename X>
class Vec3x{
	public:
	union{
		struct{
			X x,y,z;
		};
		X a[3];
	};
	inline Vec3x():x(0),y(0),z(0){}
	inline Vec3x(X a):x(a),y(a),z(a){}
	inline Vec3x(X x, X y, X z):x(x),y(y),z(z){}
	inline Vec3x(const Vec3x<X> & other): x(other.x), y(other.y), z(other.z){}
	inline virtual ~Vec3x(){}
	void operator+=(const Vec3x<X> &other){
		this->x += other.x;
		this->y += other.y;
		this->z += other.z;
	}
	void operator-=(const Vec3x<X> &other){
		this->x -= other.x;
		this->y -= other.y;
		this->z -= other.z;
	}
	void operator*=(const Vec3x<X> &other){
		this->x *= other.x;
		this->y *= other.y;
		this->z *= other.z;
	}
	void operator/=(const Vec3x<X> &other){
		this->x /= other.x;
		this->y /= other.y;
		this->z /= other.z;
	}
	
	Vec3x<X> operator+(const Vec3x<X> &other) const{
		Vec3x<X> temp(this->x + other.x, this->y + other.y, this->z + other.z);
		return temp;
	}
	Vec3x<X> operator-(const Vec3x<X> &other) const{
		Vec3x<X> temp(this->x - other.x, this->y - other.y, this->z - other.z);
		return temp;
	}
	Vec3x<X> operator*(const Vec3x<X> &other) const{
		Vec3x<X> temp(this->x * other.x, this->y * other.y, this->z * other.z);
		return temp;
	}
	Vec3x<X> operator/(const Vec3x<X> &other) const{
		Vec3x<X> temp(this->x / other.x, this->y / other.y, this->z / other.z);
		return temp;
	}
	
	bool operator<(const Vec3x<X> &other) const{
		return (this->x < other.x )||(this->x == other.x && this->y < other.y)||(this->x == other.x && this->y == other.y && this->z < other.z);
	}
	bool operator<=(const Vec3x<X> &other) const{
		return (this->x <= other.x )||(this->x == other.x && this->y <= other.y)||(this->x == other.x && this->y == other.y && this->z <= other.z);
	}
	bool operator>(const Vec3x<X> &other) const{
		return (this->x > other.x )||(this->x == other.x && this->y > other.y)||(this->x == other.x && this->y == other.y && this->z > other.z);
	}
	bool operator>=(const Vec3x<X> &other) const{
		return (this->x >= other.x )|| (this->x == other.x && this->y >= other.y)||(this->x == other.x && this->y == other.y && this->z >= other.z);
	}
	bool operator==(const Vec3x<X> &other) const{
		return (this->x == other.x ) && (this->y == other.y) && (this->z == other.z);
	}
	bool operator!=(const Vec3x<X> &other) const{
		return (this->x != other.x ) || (this->y != other.y) || (this->z != other.z);
	}
	
	X length() const{
		X length;
		length = std::sqrt(this->x*this->x+this->y*this->y+this->z*this->z);
		return length;
	}
	
	/**
	 * Normalizes this vector and returns it as a copy
	 * 
         * @return the normalized vector
         */
	Vec3x<X> normalize() const{
		Vec3x<X> temp(*this);
		return temp/temp.length();
	}
	
	/**
	 * 
         * @param other 
         * @return 
         */
	Vec3x<X> deltaSquared(const Vec3x<X> &other) const{
		return Vec3x<X>(other.x - this->x, other.y - this->y, other.z - this->z).normalize();
	}
	
	void operator*=(X mul) {
		this->x *= mul;
		this->y *= mul;
		this->z *= mul;
	}
};
typedef Vec3x<float> Vec3f;
typedef Vec3x<int> Vec3i;

#endif	/* UTILITY_H */
