#include "Animator.h"
#include <math.h>
#include <cmath>

Animator::Animator(){
	_aniEnum = ResourceManager::WALK_0;
	_walkId = 0;
}

Animator::~Animator(){
}

bool Animator::animate( const Vec2f &pos ){
	if ( pos == _lastPos ) {
		return false;
	}
	
	float angle = atan2f(pos.y-_lastPos.y, pos.x-_lastPos.x);
	int ani = _findClosestSpriteRotation(angle);

	_aniEnum = static_cast<ResourceManager::animation>( ani );
	
	_lastPos = pos;
	_walkId++;
	
	return true;
}
/**
 * Returns the sprite rotation integer for the given angle, as defined in ResourceManager::animation.
 * 
 * @param angle in radians
 * @return sprite rotation integer
 */
int Animator::_findClosestSpriteRotation(float angle){
	constexpr float ang[] = {0,-0.785398163,-1.57079633,-2.35619449,3.14159265,-3.14159265,2.35619449,1.57079633,0.785398163};
	constexpr int defaultValue = -1;
	
	for ( int i = 0; i < 9; i++ ) {
		if( std::abs( ang[i]-angle ) <= 0.392699082 ){
			return ( i < 5)? i : i-1;
		}
	}
	
	return defaultValue;
}

ResourceManager::animation Animator::getSpriteEnum(){
	return _aniEnum;
}

int Animator::getAnimationId(){
	return _walkId;
}
