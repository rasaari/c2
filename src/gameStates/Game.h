#ifndef TEST_H
#define	TEST_H

#include "../GameState.h"
#include "../Scene.h"
#include "../object/NPC.h"
#include "../object/Player.h"
//#include <luabind/luabind.hpp>

class GameState_Game: public GameState {
private:
	Player*					_player;
	Light*					_testLight; 
	Light*					_spinningLight;
	
	bool					_followPlayer;
	
public:
	Scene					scene;
	std::vector<NPC*>		dudes;
	
	void					render();
	void					update();
	void					suspend();
	void					resume();
	void					handleEvent( SDL_Event* event );
	void					init();
	
	void					loadMap( std::string );
	
	Player*					getPlayer();
	Scene*					getScene();
};


#endif	/* TEST_H */

