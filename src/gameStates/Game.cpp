#include "Game.h"
#include "../Engine.h"
#include "../GameStateManager.h"
#include "../Console.h"
#include "../XMLTilemapImporter.h"
//#include "../lua.h"

void GameState_Game::render() {
	scene.render();
}

float				lightSpin;

void GameState_Game::update() {
	//float speed = 2*Engine::appSpeed();
	lightSpin += 0.01*Engine::appSpeed();
	_spinningLight->setDirection(
		cosf( lightSpin ), 
		sinf( lightSpin ), 
		-0.15 
	);
	
	_player->update();
	
	if(Engine::keyHit(SDLK_F6)){
		while(!this->dudes.empty()){
			this->dudes.pop_back();
		}
		
		scene.clear();
	}

	if(Engine::keyHit(SDLK_F7)){
		Engine::setSimpleShader(true);
	}
	else if(Engine::keyHit(SDLK_F8)){
		Engine::setSimpleShader(false);
	}
	
	if(Engine::keyHit(SDLK_ESCAPE)){
		Engine::quit(std::string("Bye."));
	}

	for(int i=0;i<this->dudes.size();i++){
		this->dudes[i]->gotoWaypoint( this->scene.map );
	}
	/*
	for(int i=0;i<this->dudes.size();i=i+2){
		//this->dudes[i]->gotoWaypoint();
		NPC::gotoWaypoint2(this->dudes[i], this->dudes[i+1]);
	}
	*/
	//NPC::gotoWaypoint3(&(this->dudes));
	//std::cout << time_span.count() << "\n";

}

void GameState_Game::suspend() {}

void GameState_Game::resume(){}

void GameState_Game::handleEvent(SDL_Event* event) {
}

void GameState_Game::init() {
    this->scene.setCamera( {400.0f, 300.0f} );
	
    std::string mapToLoad = Engine::getTestMap();
    if(mapToLoad == "") {
	mapToLoad = "map";
    }

    Player* player = new Player( 48, 96, "cool" );
    player->setPosition( 0, 0 );

    this->hasScene = true;

    //camera follows player by default
    _followPlayer = true;

    _player = player;
    scene.insert( _player );
    scene.player = _player;
    loadMap( mapToLoad );

    _spinningLight = new Light( 0, 0, 50 );
    _spinningLight->setOmni( false );
    _spinningLight->setAtt( 0.00001 );
    _spinningLight->setPosition( -74, -448 );
    scene.addLight( _spinningLight );

    _testLight = new Light( 0, 0, 96 );
    //_testLight->setMode( false );
    _testLight->setAtt( 0.0001 );
    _testLight->setColor( 1, 1, 1 );

    scene.addLight( _testLight );
	
	_player->bindLight(_testLight);
	_player->bindScene(&scene);
	
    /*
	Light* omni1;
	for(int i=0; i<1; i++){
		omni1 = new Light( 100+48*31*i, 300, 96 );
		omni1->setAtt( 0.00004 );

		this->scene.addLight( omni1 );
	}
	/*
	for(int i=0; i<100; i++){
		omni1 = new Light( 0, 0 ,96 );
		omni1->setPosition( -300+96*i , 100 );
		omni1->setAtt( 0.0001 );

		this->scene.addLight( omni1 );
	}
*/
}

void GameState_Game::loadMap( std::string mapToLoad ) {
	XMLTilemapImporter xmlIn;
	
	if(xmlIn.import(mapToLoad, scene) == false) {
        Engine::quit(std::string("Failed to load \""+mapToLoad+".c2xmap\". Quitting gracefully."));
    }
	
	//scene.map.generateRenderable();

    for(int i = 0; i < this->dudes.size(); i++) {
        this->dudes[i]->setPosition(0,0);
        this->dudes[i]->clearPath();
    }

    /*
    //try to compile and run scriptfile related to the current map
    std::string scriptPath = "./data/maps/"+mapToLoad+".lua";

    if(luaL_dofile(luaState, scriptPath.c_str())) {
	    Console::log("Error: " + std::string(lua_tostring(luaState,-1)));
	    lua_pop(luaState, 1);
    }
    */
}

Player* GameState_Game::getPlayer() {
    return _player;
}

Scene* GameState_Game::getScene() {
    return &this->scene;
}