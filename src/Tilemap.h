#ifndef _TILEMAP_H
#define _TILEMAP_H

#define NO_SDL_GLEXT
#include <GL/glew.h>
#include <vector>
#include <map>
#include <fstream>
#include <iostream>
#include <algorithm>
#include "object/Tile.h"

#define MAJOR_VERSION 0
#define MINOR_VERSION 1

/**
 * Class for storing c2map tilemap.
 *
 */
class Tilemap {
public:
	char					majorVersion;
	char					minorVersion;
	int						tileSize;
	
	std::string				mapName;
	
							Tilemap(char world, char area);
							Tilemap();
	virtual					~Tilemap();

	void					render();
	bool					importC2Map(std::string name);
	
	Tile*					getTileAt(const Vec2f &pos, float layer);
	bool					findFloorAt(const Vec2f &pos) const;
	
	void					insertTile( Tile tile );
	bool					insertTileset( std::string tilesetName );
	
	void					clear();
private:
	struct TilemapBatch{
		int id, begin, size;
	};
	
	/**
	* Struct for sorting Tilemap::tiles.
	*/
	struct TileSortStruct{
		std::map<std::string,TilemapBatch>* vec;
		
		TileSortStruct(std::map<std::string,TilemapBatch>* p);
 
		bool operator() (const Tile &a, const Tile &b);
	};
	bool					_isReady;

	std::vector<Tile>			_tiles;
	std::vector<std::string>		_tilesets;
	std::map<Vec2f,bool,Vec2f>		_floorData;
	typedef std::map<Vec2f,bool,Vec2f>::const_iterator floorIt;
	
	void					_insert(int x, int y, char layer, int id, const std::string &tileset, int flags);
	void					_countTilesetDimensions(std::vector<int>*);
	void					_sortTiles();
	void					_shiftWallCenters();
	void					_createFloorMap();
	void					_generateRenderable();
	
	std::vector<Tile>::iterator _findLowerWall(const Vec2f &pos);
};

#endif