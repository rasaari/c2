-- example initialization file
function print(...)
	local n = select("#",...)
	local line = ""

	for i = 1,n do
		local v = tostring(select(i,...))
		line = line .. v
	end

	echo(line)
end

function dudes(count)
	for i = 1, count do
		addDude(0, 0)
	end
end
