#version 120
#define MAX_LIGHTS 4

uniform sampler2D Difftexture0;
uniform sampler2D Difftexture1;
uniform sampler2D Difftexture2;
uniform sampler2D Difftexture3;

uniform int LightCount;
uniform float LightsArray[104];

varying vec2 Texcoord;
varying float Texunit;

varying vec3 normal;
varying vec3 lightDir[MAX_LIGHTS];
varying float att[MAX_LIGHTS];

const float cos_outer_cone_angle = 0.85; // 36 degrees

void main (void){
	int index = int(Texunit);
	vec4 diffuse;
		
	if(index == 0){
		diffuse = texture2D(Difftexture0, Texcoord);
	}
	else if(index == 1){
		diffuse = texture2D(Difftexture1, Texcoord);
	}
	else if(index == 2){
		diffuse = texture2D(Difftexture2, Texcoord);
	}
	else if(index == 3){
		diffuse = texture2D(Difftexture3, Texcoord);
	}
	else{
		diffuse = vec4(0.5,0.5,0.5,1.0);
	}
	
	if(diffuse.a < 0.4){
		discard;
	}
	vec4 final_color = (diffuse * vec4(0.1,0.1,0.1,1.0));
	int offset;
	float attOrSpot;
	vec3 N,L,D;
	float lambertTerm;
	float cos_cur_angle,cos_inner_cone_angle,cos_inner_minus_outer_angle;
	N = normalize(normal);
	
	// Light 1
	if(LightsArray[12] != 0){
		if(att[0] >= 0.02){
			L = normalize(lightDir[0]);
			lambertTerm = dot(N,L);
			
			if(lambertTerm > 0.0){
				if(LightsArray[11] == 0){
					attOrSpot = att[0];
				}
				else{
				
					D = normalize(vec3(LightsArray[8],LightsArray[9],LightsArray[10]));
					cos_cur_angle = dot(-L, D);
					cos_inner_cone_angle = 0.95;
					cos_inner_minus_outer_angle = cos_inner_cone_angle - cos_outer_cone_angle;

					attOrSpot = clamp((cos_cur_angle - cos_outer_cone_angle) / cos_inner_minus_outer_angle, 0.0, 1.0);
					attOrSpot = attOrSpot*att[0];
				}
				final_color += diffuse * vec4(LightsArray[4],LightsArray[5],LightsArray[6],1) * lambertTerm * attOrSpot;
			}
		}
		// Light 2
		if(LightsArray[25] != 0){
			if(att[1] >= 0.02){
				L = normalize(lightDir[1]);
				lambertTerm = dot(N,L);
				
				if(lambertTerm > 0.0){
					if(LightsArray[24] == 0){
						attOrSpot = att[1];
					}
					else{
					
						D = normalize(vec3(LightsArray[21],LightsArray[22],LightsArray[23]));
						cos_cur_angle = dot(-L, D);
						cos_inner_cone_angle = 0.95;
						cos_inner_minus_outer_angle = cos_inner_cone_angle - cos_outer_cone_angle;
						
						attOrSpot = clamp((cos_cur_angle - cos_outer_cone_angle) / cos_inner_minus_outer_angle, 0.0, 1.0);
						attOrSpot = attOrSpot*att[1];
					}
					final_color += diffuse * vec4(LightsArray[17],LightsArray[18],LightsArray[19],1) * lambertTerm * attOrSpot;
				}
			}
			
			// Light 3
			if(LightsArray[38] != 0){
				if(att[2] >= 0.02){
					L = normalize(lightDir[2]);
					lambertTerm = dot(N,L);
					
					if(lambertTerm > 0.0){
						if(LightsArray[37] == 0){
							attOrSpot = att[2];
						}
						else{
						
							D = normalize(vec3(LightsArray[34],LightsArray[35],LightsArray[36]));
							cos_cur_angle = dot(-L, D);
							cos_inner_cone_angle = 0.95;
							cos_inner_minus_outer_angle = cos_inner_cone_angle - cos_outer_cone_angle;
							
							attOrSpot = clamp((cos_cur_angle - cos_outer_cone_angle) / cos_inner_minus_outer_angle, 0.0, 1.0);
							attOrSpot = attOrSpot*att[2];
						}
						final_color += diffuse * vec4(LightsArray[30],LightsArray[31],LightsArray[32],1) * lambertTerm * attOrSpot;
					}
				}
				
				// Light 4
				if(LightsArray[51] != 0){
					if(att[3] >= 0.02){
						L = normalize(lightDir[3]);
						lambertTerm = dot(N,L);
						
						if(lambertTerm > 0.0){
							if(LightsArray[50] == 0){
								attOrSpot = att[3];
							}
							else{
							
								D = normalize(vec3(LightsArray[47],LightsArray[48],LightsArray[49]));
								cos_cur_angle = dot(-L, D);
								cos_inner_cone_angle = 0.95;
								cos_inner_minus_outer_angle = cos_inner_cone_angle - cos_outer_cone_angle;
								
								attOrSpot = clamp((cos_cur_angle - cos_outer_cone_angle) / cos_inner_minus_outer_angle, 0.0, 1.0);
								attOrSpot = attOrSpot*att[3];
							}
							final_color += diffuse * vec4(LightsArray[43],LightsArray[44],LightsArray[45],1) * lambertTerm * attOrSpot;
						}
					}
				}
			}
		}
	}
	
	gl_FragColor = final_color;
}