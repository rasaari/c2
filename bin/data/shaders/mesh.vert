#version 120
#define MAX_LIGHTS 4

attribute vec2 OBJ_Texcoord;
attribute vec3 OBJ_Position;
attribute vec3 OBJ_Center;
attribute float OBJ_TexUnit;

uniform mat4 ViewMatrix;
uniform mat3 WallMatrix;

uniform int LightCount;
uniform float LightsArray[104];

varying float Texunit;
varying vec2 Texcoord;
varying vec3 normal;
varying vec3 lightDir[MAX_LIGHTS];
varying float att[MAX_LIGHTS];

void main (void){
	vec4 pos = vec4(OBJ_Position.xyz, 1.0);
	Texcoord = OBJ_Texcoord;
	Texunit = OBJ_TexUnit;
	vec4 lpos = pos;
	bool isWall = (OBJ_Center.x == 0 && OBJ_Center.y == 0 && OBJ_Center.z == 0)?false:true;
	if(isWall == false){
		lpos.z = 2;
	}
	else{
		lpos.xyz = lpos.xyz-OBJ_Center;
		lpos.xyz = lpos.xyz*WallMatrix;
		lpos.xyz = lpos.xyz+OBJ_Center;
		
		lpos.y = lpos.y+24;
		lpos.z = lpos.z+24;
	}
	
	vec3 vVertex = vec3(gl_ModelViewMatrix * lpos);
	vec3 fakeNormal = (isWall == false)?vec3(0,0,1):vec3(0,1,0);
	normal = normalize(gl_NormalMatrix * fakeNormal);
	float d;
	
	if(LightsArray[12] != 0){
		lightDir[0] = vec3(vec4(LightsArray[0],LightsArray[1],LightsArray[2],1)*ViewMatrix) - vVertex;
		d = length(lightDir[0]);
		att[0] = 1 / (0.5 + 0.0007 * d + LightsArray[7] * d * d);
		
		if(LightsArray[25] != 0){
			lightDir[1] = vec3(vec4(LightsArray[13],LightsArray[14],LightsArray[15],1)*ViewMatrix) - vVertex;
			d = length(lightDir[1]);
			att[1] = 1 / (0.5 + 0.0007 * d + LightsArray[20] * d * d);
			
			if(LightsArray[38] != 0){
				lightDir[2] = vec3(vec4(LightsArray[26],LightsArray[27],LightsArray[28],1)*ViewMatrix) - vVertex;
				d = length(lightDir[2]);
				att[2] = 1 / (0.5 + 0.0007 * d + LightsArray[33] * d * d);
				
				if(LightsArray[51] != 0){
					lightDir[3] = vec3(vec4(LightsArray[39],LightsArray[40],LightsArray[41],1)*ViewMatrix) - vVertex;
					d = length(lightDir[3]);
					att[3] = 1 / (0.5 + 0.0007 * d + LightsArray[46] * d * d);
				}
			}
		}
	}
	
	gl_Position = gl_ModelViewProjectionMatrix*pos;
}