#version 120

uniform sampler2D Difftexture0;
uniform sampler2D Difftexture1;
uniform sampler2D Difftexture2;
uniform sampler2D Difftexture3;

varying vec2 Texcoord;
varying float Texunit;

void main (void){
	int index = int(Texunit);
	vec4 diffuse;
		
	if(index == 0){
		diffuse = texture2D(Difftexture0, Texcoord);
	}
	else if(index == 1){
		diffuse = texture2D(Difftexture1, Texcoord);
	}
	else if(index == 2){
		diffuse = texture2D(Difftexture2, Texcoord);
	}
	else if(index == 3){
		diffuse = texture2D(Difftexture3, Texcoord);
	}
	else{
		diffuse = vec4(0.5,0.5,0.5,1.0);
	}
	
	if(diffuse.a < 0.4){
		discard;
	}
	
	gl_FragColor = diffuse;
}