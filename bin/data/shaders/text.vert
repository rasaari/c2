#version 120

attribute vec2 Text_Pos;
attribute vec2 Text_UV;
attribute float Text_TexUnit;

varying vec2 Texcoord;
varying float TexUnit;

void main (void){
	vec2 poss = Text_Pos;

	vec4 pos = vec4(Text_Pos, 0, 1.0);
	Texcoord = Text_UV;
	TexUnit = Text_TexUnit;
	
	gl_Position = gl_ModelViewProjectionMatrix*pos;
}