#version 120

attribute vec2 OBJ_Texcoord;
attribute vec3 OBJ_Position;
attribute float OBJ_TexUnit;

varying float Texunit;
varying vec2 Texcoord;

void main (void){
	vec4 pos = vec4(OBJ_Position.xyz, 1.0);
	Texcoord = OBJ_Texcoord;
	Texunit = OBJ_TexUnit;
	
	gl_Position = gl_ModelViewProjectionMatrix*pos;
}