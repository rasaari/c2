CC=g++
OS=32
LDFLAGS =
CFLAGS = -std=c++11 -O2 -fpic -D__C2_LIB__
SHARED_EXT=so
INCLUDE =

MINGW_LDFLAGS = -L./lib -lmingw32 -lboost_system -lboost_filesystem -lboost_chrono -lSDL_gfx ./lib/SDL_image.lib -lSDLmain -lSDL.dll -lglew32.dll -lglu32 -lopengl32 -g -lm
LINUX_LDFLAGS = -lSDL -lSDL_gfx -lSDL_image -lGLEW -lGLU -lGL -lboost_system -lboost_filesystem -lboost_chrono -lm

FIND_TOOL=find
SYS := $(shell $(CC) -dumpmachine)
ifneq (, $(findstring linux, $(SYS)))
   FIND_TOOL = find
   LDFLAGS = $(LINUX_LDFLAGS)
else ifneq (, $(findstring mingw, $(SYS)))
   FIND_TOOL = /bin/find
   LDFLAGS = $(MINGW_LDFLAGS)
   INCLUDE = -I./include
    SHARED_EXT = dll
endif

LBITS := $(shell uname -m)
ifeq ($(LBITS), x86_64)
   OS = 64
endif

SRCS = $(shell $(FIND_TOOL) ./src -type f -name "*.cpp" |tr "\\n" " ")
OBS = $(shell $(FIND_TOOL) ./src -type f -name "*.cpp" |sed "s/.cpp/.o/g" |tr "\\n" " ")

all: lib

lib: $(OBS)
	$(CC) -shared $(OBS) $(LDFLAGS) -o ./bin/libC2.$(SHARED_EXT)

%.o: %.cpp
	$(CC) $(CFLAGS) -c $(INCLUDE) $< -o $@

clean:
	@echo $(OBS) |xargs rm -f
